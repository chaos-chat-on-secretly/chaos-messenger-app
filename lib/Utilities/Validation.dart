bool validatorUserId(String user_id){

  user_id = user_id.trim();

  bool valid = false;

  if(user_id.length<=255){

    RegExp re = RegExp(r"^@[a-z0-9\.=\-\/_]{1,}:.{1,}\..{1,}");

    valid = re.hasMatch(user_id);

  }

  return valid;

}

String validatorUsername(String value){

  RegExp regExp = new RegExp(r'^[a-z0-9=._/-]+$');

  if(value.isEmpty){
    return 'Please enter username';
  }
  else if(!regExp.hasMatch(value)){
    return 'Invalid Username';
  }
  return null;
}

String validatorServerURL(String value){

  if(value.isEmpty){
    return 'Please enter valid url';
  }
  else if(!Uri.parse(value).isAbsolute){
    return 'Invalid Url';
  }
  return null;
}