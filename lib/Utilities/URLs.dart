import 'API/Authentication.dart';
import 'package:shared_preferences/shared_preferences.dart';

//Default Server
final defaultServer = "https://matrix.org";

String _serverUrl = defaultServer;

loadServerDetails() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  _serverUrl = prefs.get("server_url") ?? defaultServer;
}

String serverUrl(){
  return _serverUrl;
}

setServerUrl(String url) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("server_url", url);
  _serverUrl=url;
}

//Other URLs

String filterUrl(){
  return serverUrl() + "/_matrix/client/r0/user/$user_id/filter";
}

String syncWithFilterUrl(String filterId){
  return serverUrl() + "/_matrix/client/r0/sync?filter=$filterId";
}

String syncWithNextBatchUrl(String next_batch){
  return serverUrl() + "/_matrix/client/r0/sync?since=$next_batch";
}

String signOutUrl(){
  return serverUrl() + "/_matrix/client/r0/logout";
}

String updateAccountDataUrl(String accountDataType){
  return serverUrl() + "/_matrix/client/r0/user/$user_id/account_data/$accountDataType";
}

String getAccountDataUrl(String accountDataType){
  return serverUrl() + "/_matrix/client/r0/user/$user_id/account_data/$accountDataType";
}

String authTypeUrl(){
  return serverUrl() + "/_matrix/client/r0/login";
}

String signInUrl(){
  return serverUrl() + "/_matrix/client/r0/login";
}

String signUpUrl(){
  return serverUrl() + "/_matrix/client/r0/register";
}

String sendMessageUrl(String room_id){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/send/m.room.message";
}

//Rooms

String createRoomUrl(){
  return serverUrl() + "/_matrix/client/r0/createRoom";
}

String leaveRoomUrl(String room_id){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/leave";
}

String kickUserUrl(String room_id){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/kick";
}

String getRoomMembersUrl(String room_id){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/joined_members";
}

String inviteUserUrl(String room_id){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/invite";
}

String getRoomNameUrl(String room_id){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/state/m.room.name";
}

String getRoomAvatarUrl(String room_id){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/state/m.room.avatar";
}

String olderEventsUrl(String room_id, String prev_batch){
  return serverUrl() + "/_matrix/client/r0/rooms/$room_id/messages?dir=b&from=$prev_batch";
}

String getUserProfileUrl(String user_id){
  return serverUrl() + "/_matrix/client/r0/profile/$user_id";
}

String getJoinedRoomsUrl(){
  return serverUrl() + "/_matrix/client/r0/joined_rooms";
}

String mxcToHttpUrl(String mxcURL){

  if(mxcURL != null){
    return  mxcURL.replaceFirst("mxc://", serverUrl() + "/_matrix/media/r0/download/");
  }
  return null;
}

String contentUploadUrl(String fileName){
  return serverUrl() + "/_matrix/media/r0/upload";
}

