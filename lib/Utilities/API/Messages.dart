import 'dart:async';
import 'dart:convert';
import 'package:chaos/Models/SendMessage.dart';
import 'package:chaos/Utilities/API/Events.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'Authentication.dart';

const SEND_MESSAGE_SUCCESS = "SEND_MESSAGE_SUCCESS";
const SEND_MESSAGE_ERROR= "SEND_UNKNOWN_ERROR";

Future<String> sendEvent(String room_id, ChatMessage message) async {

  // Get event sending url
  String url = sendMessageUrl(room_id);

  Map<String, dynamic> data = _buildEventData(room_id, message);

  try {

    http.Response response =
    await http.post(Uri.parse(url) , headers: {
      "Content-Type": "application/json" ,
      "authorization": "Bearer $token"
    } ,
        body: json.encode(data)
    );

    if (response.statusCode == 200) {
      String event_id = SendMessageModel.fromJson(json.decode(response.body)).event_id;

      return event_id;

    } else {
      print( "Send Event: ERROR ${response.statusCode}: ${response.body}");
      print(data);
      return SEND_MESSAGE_ERROR;
    }
  }
  catch(err){
    print("Send Event: ${err.toString()}");
    return SEND_MESSAGE_ERROR;
  }
}

Map<String, dynamic> _buildEventData(String room_id, ChatMessage message){

  Map<String, dynamic> data = {
    "msgtype": message.eventType
  };

  switch(message.eventType){
    case mText:
      data["body"] = message.text;
      break;
    case mImage:
      data["body"] = "image attachment";
      data["url"] = message.customProperties["mxcURL"];
      break;
  }

  return data;

}
