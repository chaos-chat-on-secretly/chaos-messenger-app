import 'dart:async';
import 'dart:convert';
import 'package:chaos/Models/ErrorHandlingModel.dart';
import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:chaos/Utilities/URLs.dart';
import '../Authentication.dart';

const M_LIMIT_EXCEEDED = "M_LIMIT_EXCEEDED";
const  LEAVE_UNKNOWN_ERROR = "UNKNOWN_ERROR";
const  KICK_UNKNOWN_ERROR = "UNKNOWN_ERROR";

const LEAVE_SUCCESS = "LEAVE_SUCCESS";
const M_FORBIDDEN = "M_FORBIDDEN";
const KICK_SUCCESS = "KICK_SUCCESS";

Future leaveRoom(String room_id, RoomListModel roomListModel) async{

  String url = leaveRoomUrl(room_id);

  try{
    http.Response response =
    await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        }
    );
    if (response.statusCode == 200) {
      await roomListModel.removeRoom(room_id);
      return LEAVE_SUCCESS;

    }
    else{
      ErrorHandlingModel errorsResponse = ErrorHandlingModel.fromJson(
          json.decode(response.body));
      if (errorsResponse.errcode == M_LIMIT_EXCEEDED) {
        return errorsResponse.error;
      }
      else {
        return 'Unhandled Error:' + errorsResponse.error;
      }
    }
  }
  catch(err){
    return LEAVE_UNKNOWN_ERROR;
  }
}

Future kickUser(String room_id, String user_id) async{
  String url = kickUserUrl(room_id);
  Object data={
    "user_id":user_id
  };
  try{
    http.Response response =
    await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        },
        body:json.encode(data)
    );
    if (response.statusCode == 200) {
      final memberListBox = await Hive.openBox(room_id);
      List<dynamic> memberList=memberListBox.get('Members');
      memberList.removeWhere((item) => item.user_id == user_id);
      memberListBox.put("Members",memberList);
      return KICK_SUCCESS;

    }
    else{
      ErrorHandlingModel errorsResponse = ErrorHandlingModel.fromJson(
          json.decode(response.body));
      if (errorsResponse.errcode ==M_FORBIDDEN) {
        return errorsResponse.error+"because You are not an admin";
      }
      else {
        return 'Unhandled Error:' + errorsResponse.error;
      }
    }
  }
  catch(err){
    return KICK_UNKNOWN_ERROR;
  }
}