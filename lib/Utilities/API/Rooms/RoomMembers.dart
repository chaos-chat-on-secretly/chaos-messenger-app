import 'dart:async';
import 'dart:convert';
import 'package:chaos/Models/ErrorHandlingModel.dart';
import 'package:chaos/Models/Hive/Members/MatrixUser.dart';
import 'package:chaos/Models/JoinedMembers.dart';
import 'package:chaos/Models/UserProfile.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:chaos/Utilities/URLs.dart';
import '../Authentication.dart';

const M_UNSUPPORTED_ROOM_VERSION='M_UNSUPPORTED_ROOM_VERSION';
const M_BAD_JSON='M_BAD_JSON';
const M_NOT_JSON='M_NOT_JSON';
const UNKNOWN_ERROR = "UNKNOWN_ERROR";
const USER_INVITED_SUCCESS = "USER_INVITED_SUCCESS ";

Future<List<UserProfile>> getJoinedMembersFromServer(String room_id) async {

  String url = getRoomMembersUrl(room_id);

  List<UserProfile> memberList = new List<UserProfile>.empty(growable: true);

  try{
    http.Response response = await http.get(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        }
    );
    if (response.statusCode == 200) {
      JoinedMembersModel members = JoinedMembersModel.fromJson(json.decode(response.body));

      for(MapEntry e in members.joinedMembers.entries){
        memberList.add(
            UserProfile(
                user_id: e.key.toString(),
                displayname: e.value['display_name'],
                avatar_url: e.value['avatar_url']
            )
        );
      }
    }
  }
  catch(err){
    print("Network Error");
  }

  return memberList;
}

// Future<List<MatrixUser>> getJoinedMembers(String room_id) async {
//   //Open box
//   final roomBox = await Hive.openBox(room_id);
//
//   if (!roomBox.containsKey("Members")) {
//     await getJoinedMembersFromServer(room_id);
//   }
//   List<dynamic> memberList = roomBox.get("Members");
//
//   return memberList.cast<MatrixUser>();
// }

Future inviteUser(String room_id, String user_id) async {
  String url = inviteUserUrl(room_id);
  Object data={
    "user_id":user_id
  };
  try{
    http.Response response =
    await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        },
        body:json.encode(data)
    );

    if (response.statusCode == 200) {
      return USER_INVITED_SUCCESS;
    }
    else {
      ErrorHandlingModel errorsResponse = ErrorHandlingModel.fromJson(
          json.decode(response.body));

      if (errorsResponse.errcode == M_UNSUPPORTED_ROOM_VERSION) {
        return errorsResponse.error;
      }
      else if (errorsResponse.errcode == M_BAD_JSON ||
          errorsResponse.errcode == M_NOT_JSON) {
        return errorsResponse.error;
      }
      else {
        return 'Unhandled Error:' + errorsResponse.error;
      }
    }
  }
  catch(err){
    return UNKNOWN_ERROR;
  }
}
