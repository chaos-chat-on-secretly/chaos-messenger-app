import 'dart:convert';

import 'package:chaos/Models/OlderRoomEvents.dart';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Models/RoomInfo.dart';
import 'package:chaos/Models/UserProfile.dart';
import 'package:chaos/Utilities/API/AccountData.dart';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';
import 'package:chaos/Utilities/API/sharedPrefs.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:http/http.dart' as http;

Future<String> getRoomName(String room_id) async {

  String url = getRoomNameUrl(room_id);

  http.Response response = await http.get(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        "authorization": "Bearer $token"
      }
  );
  if(response.statusCode==200) {
    return RoomNameModel
        .fromJson(json.decode(response.body))
        .name;
  }
  else{
    return null;
  }
}

Future<String> getRoomAvatar(String room_id) async {

  String url = getRoomAvatarUrl(room_id);

  http.Response response = await http.get(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        "authorization": "Bearer $token"
      }
  );
  if(response.statusCode==200) {
    return RoomAvatarModel
        .fromJson(json.decode(response.body))
        .url;
  }
  else{
    return null;
  }
}

Future<RoomV2> getRoomModel(String room_id) async {
  try {
    Map<String, dynamic> mDirectAccountData = await getAccountData('m.direct');

    //get room info

    String room_name;
    String room_avatar;
    bool is_direct=false;

    room_name = await getRoomName(room_id);
    room_avatar = await getRoomAvatar(room_id);

    //Check if direct chat
    for (MapEntry<String, dynamic> direct in mDirectAccountData.entries) {
      if(direct.value.contains(room_id)){
        is_direct=true;
        UserProfile directChatRoomMember = await getUserProfile(direct.key);
        room_name ??= directChatRoomMember?.displayname;
        room_avatar ??= directChatRoomMember?.avatar_url;
        break;
      }
    }

    RoomV2 room = RoomV2(room_id);
    room.room_name = room_name;
    room.is_direct = is_direct;
    room.room_avatar = room_avatar;

    return room;

  } catch(e) {
    return RoomV2(room_id);
  }

}

Future<List<dynamic>> getOlderRoomEvents(String room_id) async {

  String prev_batch = await getPrevBatchFromLocalStorage(room_id);

  String url = olderEventsUrl(room_id, prev_batch);

  http.Response response = await http.get(
    Uri.parse(url),
    headers: {
      "Content-Type": "application/json",
      "authorization": "Bearer $token"
    },
  );
  if (response.statusCode == 200) {
    OlderRoomEvents data =  OlderRoomEvents.fromJson(json.decode(response.body));
    await savePrevBatchToLocalStorage(room_id, data.end);
    return data.events;
  }

  return List.empty(growable: true);
}



Future<void> getJoinedRooms() async {

  String url = getJoinedRoomsUrl();

  http.Response response = await http.get(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        "authorization": "Bearer $token"
      }
  );

  if (response.statusCode == 200) {

  }else{
    print(response.body);
  }
}

Future<UserProfile> getUserProfile(String user_id) async {

  UserProfile user = await getUserProfileFromLocalStorage(user_id);

  if (user.runtimeType != UserProfile) {
    String url = getUserProfileUrl(user_id);

    try {
      http.Response response = await http.get(
          Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "authorization": "Bearer $token"
          }
      );

      if (response.statusCode == 200) {
        user = UserProfile.fromJson(json.decode(response.body));
        await saveUserProfile(user_id, user);
      }else{
        user = UserProfile(user_id: user_id);
      }

    } on Exception catch (e) {
      print("getUserProfileFromServer: ${e.toString()}");
    }

  }

  String currentUser = await getStringSharedPrefs("user_id");

  if (user.user_id == currentUser) user.displayname = "You";

  return user;

}