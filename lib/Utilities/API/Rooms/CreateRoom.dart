import 'dart:async';
import 'dart:convert';
import 'package:chaos/Models/CreateNewRoom.dart';
import 'package:chaos/Models/ErrorHandlingModel.dart';
import 'package:chaos/Models/Hive/Room/Room.dart';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:chaos/Utilities/API/AccountData.dart';
import 'package:http/http.dart' as http;
import 'package:chaos/Utilities/URLs.dart';
import '../Authentication.dart';

//Errors
const M_ROOM_IN_USE='M_ROOM_IN_USE';
const M_INVALID_ROOM_STATE='M_INVALID_ROOM_STATE';
const M_UNSUPPORTED_ROOM_VERSION='M_UNSUPPORTED_ROOM_VERSION';
const M_BAD_JSON='M_BAD_JSON';
const M_NOT_JSON='M_NOT_JSON';
const CREATE_UNKNOWN_ERROR = "UNKNOWN_ERROR";

//Constants
const ROOM_CREATED_SUCCESS = "ROOM_CREATED_SUCCESS";

Future createRoom(String name, bool is_direct, List<String> invite, RoomListModel roomListModel) async {
  String url = createRoomUrl();

  Map<String, dynamic> data = {
    "preset": "private_chat",
    "invite": invite,
    "is_direct": is_direct,
    "creation_content": {
      "m.federate": true
    }
  };

  if (!is_direct) {
    data['name'] = name;
  }

  try {
    http.Response response = await http.post(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        },
        body: json.encode(data)
    );
    if (response.statusCode == 200) {
      CreateRoomModel parsedResponse = CreateRoomModel.fromJson(json.decode(response.body));

      String room_id = parsedResponse.room_id;

      RoomV2 room = RoomV2(room_id);
      room.room_name = name;
      room.is_direct = is_direct;

      //Add to conversation list
      roomListModel.addRoom(room);

      if(is_direct){
        _updateDirectChatAccountData(invite[0], room_id);
      }

      return ROOM_CREATED_SUCCESS;

    }
    else {
      ErrorHandlingModel errorsResponse = ErrorHandlingModel.fromJson(
          json.decode(response.body));
      if (errorsResponse.errcode == M_ROOM_IN_USE) {
        return errorsResponse.error;
      }
      else if (errorsResponse.errcode == M_INVALID_ROOM_STATE) {
        return errorsResponse.error;
      }
      else if (errorsResponse.errcode == M_UNSUPPORTED_ROOM_VERSION) {
        return errorsResponse.error;
      }
      else if (errorsResponse.errcode == M_BAD_JSON ||
          errorsResponse.errcode == M_NOT_JSON) {
        return errorsResponse.error;
      }
      else {
        return 'Unhandled Error:' + errorsResponse.error;
      }
    }
  }
  catch(err){
    return CREATE_UNKNOWN_ERROR+err.toString();
  }
}

void _updateDirectChatAccountData(String direct_user_id, String room_id) async {

  Map<String, dynamic> mDirect = await getAccountData("m.direct");

  if(mDirect.containsKey(direct_user_id)){
    mDirect.forEach((key, value) {
      if(key == direct_user_id){
        value.add(room_id);
      }
    });
  }else{
    mDirect.putIfAbsent(direct_user_id, () => ["$room_id"]);
  }
  updateAccountData("m.direct", mDirect);
}