import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
// import 'package:http_parser/http_parser.dart';

import '../URLs.dart';
import 'Authentication.dart';

Future<String> upload(PlatformFile file) async {
  String url = contentUploadUrl(file.name);

  try {

    // print(file.bytes);

    http.Response response = await http.post(Uri.parse(url),
        headers: {
          "Content-Type": "image/${file.extension}",
          "authorization": "Bearer $token"
        },
        body: file.bytes);

    if(response.statusCode == 200) {
      return (json.decode(response.body))["content_uri"];
    }else{
      print("upload: ${response.statusCode} - ${response.body}");
      Fluttertoast.showToast(msg: "upload: ${response.statusCode} - ${response.body}");
    }

  } catch (err) {
    Fluttertoast.showToast(msg: err.toString());
  }
}
