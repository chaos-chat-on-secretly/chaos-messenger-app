import 'package:chaos/Models/Events.dart';
import 'package:chaos/Models/Hive/Room/Room.dart';
import 'package:chaos/Models/UserProfile.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:hive/hive.dart';

Box roomListBox;

Future<void> clearLocalData() async {
  try {

    // Delete known users
    try {
      Box knownUsersBox = await Hive.openBox("known-users");
      knownUsersBox.deleteFromDisk();
    }catch(e){
      print(e.toString());
    }

    //Delete rooms
    List<dynamic> roomList = roomListBox.get("roomsV2") ?? List.empty();

    for(String room_id in roomList){
      try {
        Box roomBox = await Hive.openBox(room_id);
        roomBox.deleteFromDisk();
      }catch(e){
        print(e.toString());
        continue;
      }
    }
    roomListBox.delete('roomsV2');
  } on Exception catch (e) {
    print(e.toString());
  }
}

Future<List<Room>> getRoomsFromLocalStorage() async {
  //Open box
  roomListBox ??= await Hive.openBox("RoomList");

  List<dynamic> roomList = roomListBox.get("rooms");

  return roomList?.cast<Room>() ?? new List.empty(growable: true);
}

Future<void> saveRoomListToLocalStorage(List<Room> roomList) async {
  //Open box
  roomListBox ??= await Hive.openBox("RoomList");

  roomListBox.put("rooms", roomList);
}

Future<void> saveMessagesListToLocalStorage(String room_id, List<ChatMessage> messages) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    List<Map<String, dynamic>> messagesJson = [];

    for(ChatMessage message in messages){
      messagesJson.add(message.toJson());
    }

    await roomBox.put("messages", messagesJson);

    //Close box
    await roomBox.close();
  }catch(e){
    print("saveMessagesListToLocalStorage: "+e.toString());
  }
}

Future<void> savePrevBatchToLocalStorage(String room_id, String prev_batch) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    //Save back to local storage
    await roomBox.put("prev_batch", prev_batch);

    //Close box
    await roomBox.close();
  }catch(e){
    print("savePrevBatchToLocalStorage: "+e.toString());
  }
}

Future<String> getPrevBatchFromLocalStorage(String room_id) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    //Save back to local storage
    String prev_batch = await roomBox.get("prev_batch");

    //Close box
    await roomBox.close();

    return prev_batch;
  }catch(e){
    print("getPrevBatchFromLocalStorage: "+e.toString());
    return '';
  }
}

void saveMessageToLocalStorage(String room_id, ChatMessage message) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    //get messages from local storage
    List<dynamic> messagesJsonArray = roomBox.get("messages");
    messagesJsonArray ??= [];

    //add message to list
    messagesJsonArray.add(message.toJson());

    //Save back to local storage
    await roomBox.put("messages", messagesJsonArray);

    //Close box
    await roomBox.close();
  }catch(e){
    print("saveMessageToLocalStorage: "+e.toString());
  }
}

Future<List<ChatMessage>> getMessagesFromLocalStorage(String room_id) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    //Get messages from local storage
    List<dynamic> messagesJsonArray = roomBox.get("messages");
    messagesJsonArray ??= [];

    //Close box
    await roomBox.close();

    //Convert list to ChatMessage list
    List<ChatMessage> messages = [];

    for(dynamic messageJson in messagesJsonArray){
      messages.add(ChatMessage.fromJson(messageJson));
    }

    return messages;

  }catch(e){
    print("getMessagesFromLocalStorage: " +e.toString());
    return List<ChatMessage>.empty(growable: true);
  }
}

void saveEventToLocalStorage(String room_id, EventModel event) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    //get events from local storage
    List<dynamic> eventsJsonArray = roomBox.get("events");
    eventsJsonArray ??= [];

    //add event to list
    eventsJsonArray.add(event.toJson());

    //Save back to local storage
    await roomBox.put("events", eventsJsonArray);

    //Close box
    // await roomBox.close();
  }catch(e){
    print("saveEventToLocalStorage: "+e.toString());
  }
}

Future<List<EventModel>> getEventsFromLocalStorage(String room_id) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    //Get events from local storage
    List<dynamic> eventsJsonArray = roomBox.get("events");
    eventsJsonArray ??= [];

    //Close box
    // await roomBox.close();

    //Convert list to Events list
    List<EventModel> events = [];

    for(dynamic eventJson in eventsJsonArray){
      events.add(EventModel.fromJson(eventJson));
    }

    return events;

  }catch(e){
    return List<EventModel>.empty(growable: true);
  }
}

Future<void> saveUserProfile(String user_id, UserProfile user) async {

  user.user_id = user_id;

  try{
    //open box
    Box userProfiles = await Hive.openBox("known-users");

    //save user
    await userProfiles.put(user_id, user.toJson());

    //close box
    await userProfiles.close();

  }catch(e){
    print("saveUserProfile: "+e.toString());
  }
}

Future<UserProfile> getUserProfileFromLocalStorage(String user_id) async {

  UserProfile user;

  try{
    //open box
    Box userProfiles = await Hive.openBox("known-users");

    //get user
    user = UserProfile.fromJson(await userProfiles.get(user_id));

    //close box
    await userProfiles.close();

  }catch(e, stack){
    print("getUserProfile: "+e.toString());
    print(stack.toString());
  }

  return user;
}

//V2

Future<List<String>> roomListFromLocalStorage() async {
  //Open box
  roomListBox ??= await Hive.openBox("RoomList");

  List<dynamic> roomList = roomListBox.get("roomsV2");

  return roomList?.cast<String>() ?? new List<String>.empty(growable: true);
}

Future<void> roomListToLocalStorage(List<String> roomList) async {
  //Open box
  roomListBox ??= await Hive.openBox("RoomList");

  roomListBox.put("roomsV2", roomList);

}

Future<void> saveRoomData(String room_id, String key, dynamic value) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    //Save to local storage
    await roomBox.put(key, value);

    //Close box
    // await roomBox.close();
  }catch(e){
    print("saveRoomData: "+e.toString());
  }
}

Future<dynamic> loadRoomData(String room_id, String key) async {
  try {
    //open box
    Box roomBox = await Hive.openBox(room_id);

    dynamic data;

    //Load from local storage
    if(roomBox.containsKey(key)){
      data = await roomBox.get(key);
    }

    //Close box
    // await roomBox.close();

    //return
    return data;

  }catch(e){
    print("loadRoomData: "+e.toString());
  }
}