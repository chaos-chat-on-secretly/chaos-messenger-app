import 'dart:convert';

import 'package:chaos/Models/AccountData.dart';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> getAccountData(String accountDataType) async {

  try {
    String url = getAccountDataUrl(accountDataType);

    http.Response response = await http.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "authorization": "Bearer $token"
    });

    if(response.statusCode == 200){
      return AccountDataModel.fromJson(json.decode(response.body)).content;
    }else{
      return Map<String, dynamic>();
    }
  } on Exception catch (e) {
    print("getAccountData: ${e.toString()}");
    return Map<String, dynamic>();
  }
}

Future<void> updateAccountData(String accountDataType, Map<String, dynamic> data) async {

  String url = updateAccountDataUrl(accountDataType);

  await http.put(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        "authorization": "Bearer $token"
      },
      body: json.encode(data)
  );

}