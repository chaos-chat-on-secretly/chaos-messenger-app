import 'package:chaos/Models/Events.dart';
import 'package:chaos/Utilities/API/Rooms/RoomsInfo.dart';

//event types
const mRoomMember="m.room.member";
const mRoomMessage="m.room.message";
const mRoomPowerLevels="m.room.power_levels";
const mRoomHistoryVisibility="m.room.history_visibility";
const mRoomRedaction="m.room.redaction";
const mRoomName="m.room.name";
const mRoomAvatar="m.room.avatar";

//message types
const mText="m.text";
const mNotice="m.notice";
const mImage="m.image";
const mVideo="m.video";

Future<String> getEventSummary(Map<String, dynamic> eventJson) async {

  EventModel event = getEvent(eventJson);

  if (event.runtimeType == RoomEventModel) {
    return await getRoomEventSummary(event);
  }else if(event.runtimeType == StateEventModel){
    return await getStateEventSummary(event);
  }else{
    return "Unhandled event: ${event.type}";
  }
}

Future<String> getRoomEventSummary(RoomEventModel event) async {

  String senderDisplayName = (await getUserProfile(event.sender))?.displayname ?? event.sender;

  switch (event.type) {
    case mRoomMember:
      return "${event.type}" ;
    case mRoomMessage:
      switch (event.content["msgtype"]) {
        case mText:
          return "$senderDisplayName: ${event.content["body"]}";
          break;
        case mNotice:
          return "$senderDisplayName: ${event.content["body"]}";
          break;
        case mImage:
          return "$senderDisplayName sent an image";
          break;
        case mVideo:
          return "$senderDisplayName sent a video";
          break;
        default:
          return "Unhandled msgtype: ${event.content['msgtype']}";
      }
      break;
    case mRoomPowerLevels:
      return event.type;
    case mRoomHistoryVisibility:
      return event.type;
    case mRoomRedaction:
      return event.type;
    default:
      return "Unhandled room event: ${event.type}";
  }
}

Future<String> getStateEventSummary(StateEventModel event) async {
  String summary;
  switch (event.type) {
    case mRoomMember:

      String membership = event.content['membership'];

      String senderDisplayName = (await getUserProfile(event.sender))?.displayname ?? event.sender;

      if(membership == "invite"){
        summary = "$senderDisplayName invited ${event.content['displayname'] ?? event.state_key}" ;
      }
      else if(membership == "leave"){
        if(event.sender == event.state_key){
          summary = "$senderDisplayName rejected invite";
        }else{

          String stateDisplayName = (await getUserProfile(event.state_key)).displayname;

          summary = "$senderDisplayName revoked invitation of $stateDisplayName";
        }
      }else if(membership == "join"){
        summary = "$senderDisplayName joined the room";
      }else{
        summary = "Unhandled membership event: $membership";
      }
      break;

    case mRoomName:
      String senderDisplayName = (await getUserProfile(event.sender))?.displayname ?? event.sender;
      summary = "$senderDisplayName changed room name to ${event.content['name']}";
      break;

    case mRoomAvatar:
      String senderDisplayName = (await getUserProfile(event.sender))?.displayname ?? event.sender;
      if(event.content.isEmpty){
        summary = "$senderDisplayName removed room avatar";
      }else {
        summary = "$senderDisplayName changed room avatar";
      }
      break;

    default:
      summary = "Unhandled state event: ${event.type}";

  }

  return summary;
}

EventModel getEvent(Map<String, dynamic> event){
  if(event.containsKey('state_key')){
    return StateEventModel.fromJson(event);
  }else if(event.containsKey('event_id')){
    return RoomEventModel.fromJson(event);
  }else{
    return EventModel.fromJson(event);
  }
}