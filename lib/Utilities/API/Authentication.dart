import 'dart:async';
import 'dart:convert';
import 'package:chaos/Models/AuthType.dart';
import 'package:chaos/Models/AuthTypes/MLoginPassword.dart';
import 'package:chaos/Utilities/API/sharedPrefs.dart';
import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

// Session data
String _user_id;
String _token;

//Constants
const M_USER_ID = "m.id.user";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";

//Login types
const M_LOGIN_PASSWORD = "m.login.password";
const M_LOGIN_DUMMY = "m.login.dummy";

//Errors
const UNSUPPORTED_LOGIN_TYPE = "UNSUPPORTED_LOGIN_TYPE";
const M_LIMIT_EXCEEDED = "M_LIMIT_EXCEEDED";
const UNKNOWN_ERROR = "UNKNOWN_ERROR";
const LOGIN_ERROR = "Oops! Something went wrong, check your credentials or try again later.";

//Utils

Future<String> getAuthenticationType() async {

  http.Response rawResponse = await http.get(Uri.parse(authTypeUrl()));

  String response;

  if (rawResponse.statusCode == 200) {
    response = AuthTypeModel.fromJson(json.decode(rawResponse.body)).flow.toString();
  } else if (rawResponse.statusCode == 429) {
    response = M_LIMIT_EXCEEDED;
  }
  else{
    response = json.decode(rawResponse.body).toString();
  }

  return M_LOGIN_PASSWORD; /// TODO: Implement other login flows
}

// Session

Future<bool> sessionExists() async {
  _user_id = await getStringSharedPrefs("user_id");
  _token = await getSecureStore("token");
  return (_user_id != null && _token != null);
}

Future<void> saveSession(MLoginPasswordModel sessionData) async {
  _token = sessionData.access_token;
  _user_id= sessionData.user_id;
  await saveSecureStore("token", sessionData.access_token);
  await saveStringSharedPrefs('user_id', sessionData.user_id);
}

void clearSession() {
  //Delete session data
  if(isWeb()){
    deleteSharedPref('token');
  }else {
    FlutterSecureStorage().delete(key: 'token');
  }
}

Future<void> saveNextBatch(String next_batch) async {
  await saveStringSharedPrefs("next_batch", next_batch);
}

Future<void> saveSecureStore(String key, String value) async {
  if(isWeb()){
    await saveStringSharedPrefs(key, value);
  }else {
    await FlutterSecureStorage().write(key: key, value: value);
  }
}

Future<String> getSecureStore(String key) async {
  if(isWeb()){
    return await getStringSharedPrefs(key);
  }else {
    return await FlutterSecureStorage().read(key: key);
  }
}

//Getters
String get token => _token;
String get user_id => _user_id;

//entry points

Future<http.Response> registerUser(String username, String password) async {

  Object data = {
    "auth": {
      "type": "m.login.dummy"
    },
    "username": username,
    "password": password
  };

  var response = await http.post(Uri.parse(signUpUrl()),
      headers: {
        "Content-Type": "application/json"
      },
      body: json.encode(data)
  );

  return response;
}
