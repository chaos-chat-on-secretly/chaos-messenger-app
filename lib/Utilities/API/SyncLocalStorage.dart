import 'package:chaos/Models/Events.dart';
import 'package:chaos/Utilities/API/Events.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';

//Events

Set<String> handledEvents = new Set();

//Functions

void saveRoomsJoinEvents(Map<String, dynamic> roomsJoinEvents){

  for(String room_id in roomsJoinEvents.keys){

    Map<String, dynamic> room_data = roomsJoinEvents[room_id];
    saveRoomEvents(room_id, room_data);

  }

}

void saveRoomEvents(String room_id, Map<String, dynamic> room_events) async {

  for(String type in room_events.keys) {

    switch(type){

      case "timeline":
        saveTimeline(room_id, room_events[type]);
        break;

      case "state":
      // Map<String, dynamic> data_type = room_data[type];
        break;
      default:
      // print(type);
    }
  }
}

void saveTimeline(String room_id, Map<String, dynamic> timeline) {
  List<dynamic> events = timeline['events'];
  String prev_batch = timeline['prev_batch'] ?? '';

  if (events.isNotEmpty) {
    events.forEach((eventJson) async {
      String event_id = eventJson['event_id'];

      if (!handledEvents.contains(event_id)) {
        handledEvents.add(event_id);

        RoomEventModel event = getEvent(eventJson);

        switch (event.type) {
          case mRoomMessage:
            if (event.content['msgtype'] == mText) {
              ChatMessage message = await event.toTextMessage();

              saveMessageToLocalStorage(room_id, message);
            }
        }
      }
    });
  }

  savePrevBatchToLocalStorage(room_id, prev_batch);
}