import 'package:flutter/material.dart';

class CircularAvatar extends StatefulWidget {
  CircularAvatar({
    @required this.text,
    this.imageUrl,
    @required this.radius,
  }) : super(key: ValueKey(imageUrl));

  final String text;
  final String imageUrl;
  final double radius;

  @override
  _CircularAvatarState createState() => _CircularAvatarState();
}

class _CircularAvatarState extends State<CircularAvatar> {
  final List<Color> colors = [
    Color(0xffD48C20),
    Color(0xff4476AE),
    Color(0xff9D242E),
    Color(0xff0F44B3),
    Color(0xffC3377D),
    Color(0xffC93E17),
    Color(0xff4524B3),
    Color(0xff3B9F93),
    Color(0xff741AA7),
    Color(0xffE7B927),
    Color(0xff6CB0F6),
    Color(0xffD84954),
    Color(0xff4A7CDB),
    Color(0xffE27B5A),
    Color(0xff785CDD),
    Color(0xff78D3B9),
    Color(0xffAA62C6),
    Colors.redAccent,
    Colors.blueAccent,
    Colors.lightBlue,
    Colors.greenAccent,
    Colors.purpleAccent,
    Colors.brown
  ];

  NetworkImage _image;
  bool _checkLoaded = true;

  @override
  void initState() {
    if (widget.imageUrl != null && widget.imageUrl != "") {
      _image = NetworkImage(widget.imageUrl);
      _image
          .resolve(ImageConfiguration())
          .addListener(ImageStreamListener((ImageInfo info, bool _) {
        if (mounted) {
          setState(() {
            _checkLoaded = false;
          });
        }
      }));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
        radius: widget.radius,
        backgroundColor: !_checkLoaded
            ? Theme.of(context).colorScheme.surface
            : colors[widget.text.codeUnitAt(0) % 23],
        foregroundImage: !_checkLoaded ? _image : null,
        child: !_checkLoaded
            ? null
            : Text(widget.text[0],
                style: TextStyle(
                    fontSize: widget.radius > 50
                        ? Theme.of(context).textTheme.headline2.fontSize
                        : Theme.of(context).textTheme.headline5.fontSize,
                    color: Colors.white)));
  }
}
