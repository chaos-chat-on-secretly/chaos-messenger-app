import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

//Themes

//Light Theme
final primary = Color(0xffFEDBD0);
final primaryVariant = Color(0xff442C2E);
final secondary = Color(0xffFEEAE6); //Accent Color
final secondaryVariant = Color(0xff442C2E);
final surface = Color(0xffe4e4e4);
final background = Colors.white;
final error = Color(0xFFC5032B);
final onPrimary = Color(0xff442C2E);
final onSecondary = Color(0xff442C2E);
final onSurface = Color(0xff442C2E);
final onBackground = Color(0xff442C2E);
final onError = Colors.white;

bool theme(BuildContext context) {
  return MediaQuery.of(context).platformBrightness == Brightness.dark;
}

final textTheme = TextTheme(
  headline1: TextStyle(color: onSurface),
  headline2: TextStyle(color: onSurface),
  headline3: TextStyle(color: onSurface),
  headline4: TextStyle(color: onSurface),
  headline5: TextStyle(color: onSurface),
  headline6: TextStyle(color: onSurface),
  subtitle1: TextStyle(color: onSurface),
  subtitle2: TextStyle(color: onSurface),
  bodyText1: TextStyle(color: onSurface),
  bodyText2: TextStyle(color: onSurface),
  button: TextStyle(color: onSurface),
  overline: TextStyle(color: onSurface),
  caption: TextStyle(color: Colors.black54),
);

final lightTheme = ThemeData.light().copyWith(
    colorScheme: ColorScheme(
        primary: primary,
        primaryVariant: primaryVariant,
        secondary: secondary,
        secondaryVariant: secondaryVariant,
        surface: surface,
        background: background,
        error: error,
        onPrimary: onPrimary,
        onSecondary: onSecondary,
        onSurface: onSurface,
        onBackground: onBackground,
        onError: onError,
        brightness: Brightness.light
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        foregroundColor: MaterialStateColor.resolveWith((states) => onSurface)
      )
    ),
    brightness: Brightness.light,
    scaffoldBackgroundColor: background,
    floatingActionButtonTheme: FloatingActionButtonThemeData(backgroundColor: secondary, foregroundColor: onSurface),
    primaryColor: primary,
    primaryColorDark: onSurface,
    accentColor: onSurface,
    accentColorBrightness: Brightness.dark,
    textTheme: textTheme,
    errorColor: error,
    textSelectionTheme: TextSelectionThemeData(
        cursorColor: onSurface,
        selectionColor: secondary,
        selectionHandleColor: onSurface
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: onSurface),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: onSurface, width: 1.5),
      ),
    ),
    appBarTheme: AppBarTheme(
      actionsIconTheme: IconThemeData(color: onSurface),
      iconTheme: IconThemeData(color: onSurface),
      textTheme: textTheme,
      elevation: 3,
    ),
    cupertinoOverrideTheme: CupertinoThemeData(
      brightness: Brightness.light,
      primaryColor: primary,
      primaryContrastingColor: onSurface,
      scaffoldBackgroundColor: surface,
    )
);

//Dark Theme
final primaryDark = Color(0xffFEEAE6);
final primaryVariantDark = Color(0xff442C2E);
final secondaryDark = Color(0xffFEEAE6); //Accent Color
final surfaceDark = Color(0xff121212);
final backgroundDark = Colors.black;
final errorDark = Color(0xFFcf6679);
final onPrimaryDark = Color(0xff442C2E);
final onSecondaryDark = Color(0xff442C2E);
final onSurfaceDark = Colors.white;
final onErrorDark = Colors.white;

final textThemeDark = TextTheme(
  headline1: TextStyle(color: onSurfaceDark),
  headline2: TextStyle(color: onSurfaceDark),
  headline3: TextStyle(color: onSurfaceDark),
  headline4: TextStyle(color: onSurfaceDark),
  headline5: TextStyle(color: onSurfaceDark),
  headline6: TextStyle(color: onSurfaceDark),
  subtitle1: TextStyle(color: onSurfaceDark),
  subtitle2: TextStyle(color: onSurfaceDark),
  bodyText1: TextStyle(color: onSurfaceDark),
  bodyText2: TextStyle(color: onSurfaceDark),
  button: TextStyle(color: onSecondaryDark),
  overline: TextStyle(color: onSurfaceDark),
  caption: TextStyle(color: Colors.grey),
);

final darkTheme = ThemeData.dark().copyWith(
    colorScheme: ColorScheme(
        primary: primaryDark,
        primaryVariant: primaryVariantDark,
        secondary: secondaryDark,
        secondaryVariant: secondaryDark,
        surface: surfaceDark,
        background: backgroundDark,
        error: errorDark,
        onPrimary: onPrimaryDark,
        onSecondary: onSecondaryDark,
        onSurface: onSurfaceDark,
        onBackground: onSurfaceDark,
        onError: onErrorDark,
        brightness: Brightness.dark
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
            foregroundColor: MaterialStateColor.resolveWith((states) => onSurfaceDark)
        )
    ),
    brightness: Brightness.dark,
    scaffoldBackgroundColor: backgroundDark,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: secondaryDark, foregroundColor: onSecondaryDark),
    primaryColor: surfaceDark,
    primaryColorDark: onSurfaceDark,
    accentColor: secondaryDark,
    accentColorBrightness: Brightness.dark,
    buttonTheme: ButtonThemeData(
        buttonColor: secondaryDark, textTheme: ButtonTextTheme.primary),
    textTheme: textThemeDark,
    errorColor: errorDark,
    textSelectionTheme: TextSelectionThemeData(
        cursorColor: secondary,
        selectionColor: primaryVariantDark,
        selectionHandleColor: primaryDark
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: onSurfaceDark),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: onSurfaceDark, width: 1.5),
      ),
    ),
    appBarTheme: AppBarTheme(
      actionsIconTheme: IconThemeData(color: onSurfaceDark),
      iconTheme: IconThemeData(color: primaryDark),
      textTheme: textThemeDark,
      elevation: 3,
    ),
    cupertinoOverrideTheme: CupertinoThemeData(
        brightness: Brightness.dark,
        primaryColor: surfaceDark,
        primaryContrastingColor: onSurfaceDark,
        scaffoldBackgroundColor: backgroundDark
    )
);
