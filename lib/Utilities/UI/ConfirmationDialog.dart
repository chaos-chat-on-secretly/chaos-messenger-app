import 'package:flutter/material.dart';

Future<bool> showConfirmationDialog(BuildContext context, String title, {String description}) {
  return showDialog(
    context: context,
    builder: (BuildContext context){
      return AlertDialog(
        title: Text(title, style: Theme.of(context).textTheme.bodyText1),
        content: description != null ? Text(
          description,
          style: Theme.of(context).textTheme.bodyText2,
        ) : SizedBox(),
        actions: <Widget>[
          TextButton(
            child: Text("YES"),
            onPressed: () {
              Navigator.pop(context, true);
            },
          ),
          TextButton(
            child: Text("NO"),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
        ],
      );
    },
  );
}