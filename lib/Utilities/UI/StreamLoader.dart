import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class StreamLoader extends StatelessWidget{

  final Stream<String> stream;

  const StreamLoader({this.stream});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
          stream: stream,
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {

            Widget indicator = CircularProgressIndicator();
            String loaderText;
            bool error = false;

            if (snapshot.hasError) {
              indicator = Icon(Icons.error);
              loaderText = snapshot.error.toString();
              error = true;
            } else {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  loaderText = "waiting";
                  break;
                case ConnectionState.active:
                  loaderText = snapshot.data;
                  break;
                case ConnectionState.done:
                  indicator = Icon(Icons.done);
                  loaderText = "Done!";
                  WidgetsBinding.instance.addPostFrameCallback((_) => Navigator.pop(context));
                  break;
                case ConnectionState.none:
                  loaderText = "none";
                  break;
              }
            }
            return Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    indicator,
                    error ? Text("Oops! Something went wrong. Try Again Later.") : Container(),
                    SizedBox(height: 20),
                    Text("$loaderText..."),
                    error ? ElevatedButton(
                        child: Text("Close"),
                        onPressed: (){
                          Navigator.pop(context);
                        }
                    ) : Container()
                  ]
              ),
            );
          }
      ),
    );
  }}