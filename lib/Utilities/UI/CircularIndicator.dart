import 'package:flutter/material.dart';

Widget circularIndicator(BuildContext context){

  return SizedBox(
      width: 20,
      height: 20,
      child: CircularProgressIndicator()
  );
}