import 'package:flutter/material.dart';

Size displaySize(BuildContext context) {
  // debugPrint('Size = ' + MediaQuery.of(context).size.toString());
  return MediaQuery.of(context).size;
}

double displayHeight(BuildContext context) {
  // debugPrint('Height = ' + displaySize(context).height.toString());
  return displaySize(context).height;
}

double displayWidth(BuildContext context) {
  //debugPrint('Width = ' + displaySize(context).width.toString());
  return displaySize(context).width;
}

bool isMobile(BuildContext context){
  double deviceWidth = MediaQuery.of(context).size.shortestSide;
  if (deviceWidth > 600) {
    return false;
  }  return true;
}

double bottomSheetPadding(BuildContext context){
  if(isMobile(context)){
    return 0;
  }else{
    return (0.25 * displayWidth(context));
  }
}

bool isWeb(){
  if(identical(0, 0.0)){
    return true;
  }
  return false;
}