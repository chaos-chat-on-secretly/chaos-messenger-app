import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class FullScreenLoader extends StatelessWidget{

  /// Function that returns Future<void> or Future<Null>
  final Future<void> task;

  /// Text to be shown on the loader
  final String loadingText;

  FullScreenLoader({
    this.task,
    this.loadingText
  });

  @override
  Widget build(BuildContext context) {

    task?.then((value) => Navigator.pop(context));

    String loadingText = "${this.loadingText ?? "Loading"}...";

    return Scaffold(
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              SizedBox(height: 20),
              Text(loadingText)
            ],
          )
      ),
    );
  }
}