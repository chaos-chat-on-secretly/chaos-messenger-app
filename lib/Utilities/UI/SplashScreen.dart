import 'package:chaos/Screens/Conversations/ConversationsScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:splashscreen/splashscreen.dart';
class Splash_Screen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<Splash_Screen> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      title: new Text(
        'Welcome to the world of PRIVACY',
        style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0,color: Colors.black),
      ),
      seconds: 4,

      navigateAfterSeconds: ConversationsScreen(),

      image: Image.asset(
        'images/Chaos.gif',alignment: Alignment.bottomCenter,),
      photoSize: 200.0,
      //onClick: () => print("Flutter Egypt"),
      loaderColor: Colors.blue,
    );
  }
}