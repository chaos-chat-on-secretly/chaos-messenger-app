import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:chaos/Utilities/API/Rooms/CreateRoom.dart';
import 'package:chaos/Utilities/UI/CircularIndicator.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scoped_model/scoped_model.dart';

class CreateNewRoomScreen extends StatefulWidget {
  final List<String> inviteList;
  CreateNewRoomScreen({this.inviteList});
  @override
  _CreateNewRoomScreenState createState() => _CreateNewRoomScreenState();
}

class _CreateNewRoomScreenState extends State<CreateNewRoomScreen> {
  String roomname;

  //UI
  bool _showProgressIndicator=false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actionsIconTheme: Theme.of(context).iconTheme,
          textTheme: Theme.of(context).textTheme,
          title: Text("Create New Room"),
          leading: BackButton(),
          automaticallyImplyLeading: false,
        ),
        body: Container(
            child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 15),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: TextFormField(
                            cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                            autocorrect: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              labelText: 'Room name',
                              border: OutlineInputBorder(),
                            ),
                            //onSaved: (data) => name = data,
                            onChanged: (value) {
                              setState((){
                                roomname=value;
                              });
                            }
                        ),
                      ),
                      SizedBox(height: 15),
                      Center(
                          child: ElevatedButton(
                            child: _showProgressIndicator ? circularIndicator(context) : Text("Create Room"),
                            onPressed: () async {
                              if (!_showProgressIndicator) {

                                setState(() {
                                  _showProgressIndicator = true;
                                });

                                String response= await createRoom(
                                    roomname,
                                    false,
                                    widget.inviteList,
                                    ScopedModel.of<RoomListModel>(context)
                                );
                                if(response==ROOM_CREATED_SUCCESS ) {
                                  Navigator.pop(context);
                                }
                                else if(response==CREATE_UNKNOWN_ERROR){
                                  Fluttertoast.showToast(msg: "Network error!");
                                  setState(() {
                                    _showProgressIndicator = false;
                                  });
                                }
                                else{
                                  Fluttertoast.showToast(msg: response);
                                  setState(() {
                                    _showProgressIndicator = false;
                                  });
                                }
                              }
                            }, //callback when button is clicked
                          )
                      )
                    ]
                )
            )
        )
    );
  }
}