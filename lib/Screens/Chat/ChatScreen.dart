import 'package:animations/animations.dart';
import 'package:chaos/Models/UI/ContextMenuItem.dart';
import 'package:chaos/Scoped%20Models/Chat.dart';
import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:chaos/Screens/Chat/BlankChatScreen.dart';
import 'package:chaos/Screens/Chat/MessageBuilders/Image.dart';
import 'package:chaos/Screens/Chat/MessageBuilders/Text.dart';
import 'package:chaos/Screens/Chat/MessageBuilders/Video.dart';
import 'package:chaos/Screens/Chat/MessagePreviews/ImagePreviews.dart';
import 'package:chaos/Screens/Profile/DirectChatProfile.dart';
import 'package:chaos/Screens/Profile/RoomProfile.dart';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:chaos/Utilities/API/content_repo.dart';
import 'package:chaos/Utilities/UI/CircularAvatar.dart';
import 'package:chaos/Utilities/UI/ConfirmationDialog.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

//import 'package:image_picker/image_picker.dart';
import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';

// import 'package:dash_chat/dash_chat.dart';
import 'package:scoped_model/scoped_model.dart';

class ChatScreen extends StatefulWidget {
  final bool isWeb;

  ChatScreen({this.isWeb});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  TextEditingController _messageEditingController;

  //Context menu items
  List<ContextMenuItem> choices = const <ContextMenuItem>[
    ContextMenuItem(title: 'Clear Chat'),
  ];

  ChatModel _chat;

  @override
  void dispose() {
    // Dispose TextEditingController
    _messageEditingController.dispose();

    super.dispose();
  }

  void onSend(ChatMessage message) async {
    String msgtype;

    if (message.image == null &&
        message.video == null &&
        message.text != null) {
      msgtype = "m.text";
    } else if (message.image != null) {
      msgtype = "m.image";
    } else if (message.video != null) {
      msgtype = "m.video";
    }

    assert(msgtype != null);

    message.eventType = msgtype;

    _chat.sendTextMessage(message);
  }

  void uploadFile() async {
    FilePickerResult result = await FilePicker.platform
        .pickFiles(allowMultiple: false, withData: true);
    if (result != null) {
      // if (isWeb()) {
      //   // TODO
      // } else {
      List<PlatformFile> images = await Navigator.push(context,
          MaterialPageRoute(builder: (context) => ImagePreviews(result.files)));

      if (images != null && images?.first != null) {
        String mxcURL = await upload(images.first);

        if (mxcURL != null) {
          String httpURL = mxcToHttpUrl(mxcURL);

          onSend(
            ChatMessage(
                text: null,
                image: httpURL,
                user: ChatUser(name: "You", uid: user_id),
                customProperties: {
                  "mxcURL": mxcURL,
                }),
          );
        } else {
          Fluttertoast.showToast(msg: "Error uploading image!");
        }
      } else {
        print("Upload file: $images");
      }
      // }
    }
  }

  void _select(ContextMenuItem choice) async {
    switch (choice.title) {
      case 'Clear Chat':
        bool confirm = await showConfirmationDialog(
            context, "Are you sure you want to clear all chats?",
            description:
                "This will only clear the chats locally and the chats can be loaded again.");
        if (confirm) _chat.clearMessages();
    }
  }

  @override
  Widget build(BuildContext context) {
    _chat = ScopedModel.of<ChatModel>(context);

    if (_chat.room_id == null) {
      return BlankChatScreen();
    } else {
      return WillPopScope(
        onWillPop: () async {
          ScopedModel.of<RoomListModel>(context).activeChat = ChatModel.empty();
          return true;
        },
        child: Scaffold(
            appBar: AppBar(
              titleSpacing: 0.0,
              textTheme: Theme.of(context).textTheme,
              actionsIconTheme: Theme.of(context).iconTheme,
              title: OpenContainer(
                closedColor: Theme.of(context).primaryColor,
                closedElevation: 0,
                closedBuilder: (_, action) {
                  return ListTile(
                    onTap: action,
                    leading: CircularAvatar(
                        text: _chat.room_name ?? _chat.room_id,
                        imageUrl: _chat.room.room_avatar,
                        radius: 20),
                    title: Text(
                      _chat.room_name ?? _chat.room_id,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                },
                openBuilder: (BuildContext context,
                    void Function({Object returnValue}) action) {
                  return _chat.is_direct
                      ? DirectChatProfile(_chat.room)
                      : RoomProfile(_chat.room);
                },
              ),
              leading: isMobile(context)
                  ? BackButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        ScopedModel.of<RoomListModel>(context).activeChat =
                            ChatModel.empty();
                      },
                    )
                  : null,
              automaticallyImplyLeading: false,
              actions: <Widget>[
                PopupMenuButton<ContextMenuItem>(
                  onSelected: _select,
                  icon: Icon(Icons.more_vert),
                  itemBuilder: (BuildContext context) {
                    return choices.map((ContextMenuItem item) {
                      return PopupMenuItem<ContextMenuItem>(
                        value: item,
                        child: Text(item.title,
                            style: Theme.of(context).textTheme.bodyText1),
                      );
                    }).toList();
                  },
                ),
              ],
            ),
            body: SafeArea(
              child: ScopedModelDescendant<ChatModel>(
                builder: (context, _, chatModel) {
                  _messageEditingController = TextEditingController(
                      text: _chat.room.draftPresent ? _chat.room.draft : "");

                  return ChaosChat(
                    // messageContainerPadding: EdgeInsets.only(bottom: 5),
                    key: ValueKey(_chat.room_id),
                    isWeb: widget.isWeb,
                    inverted: true,
                    sendOnEnter: true,
                    textInputAction: TextInputAction.send,
                    user: ChatUser(name: "You", uid: user_id),
                    messages: chatModel.messages.reversed.toList(),
                    onLoadEarlier: () {
                      _chat.loadOlderEvents();
                    },
                    messageBuilder: _messageBuilder,
                    text: _messageEditingController.text,
                    textController: _messageEditingController,
                    onTextChange: (value) {
                      setState(() {
                        _chat.room.draftPresent = value.length > 0;
                        _chat.room.draft = value;
                      });
                    },
                    inputMaxLines: 20,
                    inputCursorColor:
                        Theme.of(context).textSelectionTheme.cursorColor,
                    inputContainerStyle: BoxDecoration(
                      color: Theme.of(context).colorScheme.background,
                    ),
                    inputToolbarPadding: EdgeInsets.symmetric(vertical: 5),
                    inputDecoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10),
                      fillColor: Theme.of(context).colorScheme.surface,
                      isDense: true,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide: BorderSide.none,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide: BorderSide.none,
                      ),
                      hintText: "Type your message here...",
                    ),
                    onSend: onSend,
                    trailing: <Widget>[
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: uploadFile,
                      )
                    ],
                  );
                },
              ),
            )),
      );
    }
  }

  Widget _messageBuilder(ChatMessage message) {
    final isCurrentUser = user_id == message.user.uid;

    assert(message.id != null);

    if (message.image != null) {
      return ImageMessageBuilder(message, isCurrentUser);
    } else if (message.video != null) {
      return VideoMessageBuilder(message, isCurrentUser);
    } else {
      return TextMessageBuilder(message, isCurrentUser);
    }
  }
}
