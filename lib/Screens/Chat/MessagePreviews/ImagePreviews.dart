import 'dart:io';

import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImagePreviews extends StatefulWidget {
  final List<PlatformFile> images;

  ImagePreviews(this.images);

  @override
  _ImagePreviewsState createState() => _ImagePreviewsState();
}

class _ImagePreviewsState extends State<ImagePreviews> {
  int currentImage = 0;
  PageController controller;

  @override
  void initState() {
    controller = PageController();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        leading: BackButton(
          color: Colors.white,
        ),
        brightness: Brightness.dark,
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.transparent,
        bottomOpacity: 0.0,
      ),
      body: PageView.builder(
          controller: controller,
          itemCount: widget.images.length,
          onPageChanged: (page) {
            setState(() {
              currentImage = page;
            });
          },
          itemBuilder: (context, int) {
            return InteractiveViewer(
              child: SafeArea(
                child: Center(
                  child: Image.memory(
                    widget.images[int].bytes,
                    height: double.infinity,
                    width: double.infinity,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.send),
        onPressed: () {
          Navigator.pop(context, widget.images);
        },
      ),
      bottomNavigationBar: SizedBox(
        height: displayWidth(context) / 6,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: widget.images.length,
            itemBuilder: (context, position) {
              return GestureDetector(
                onTap: () {
                  controller.animateToPage(position,
                      duration: Duration(milliseconds: 250),
                      curve: Curves.decelerate);
                },
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        border: Border.all(
                            color: currentImage == position
                                ? Colors.white
                                : Colors.black,
                            width: currentImage == position ? 2.0 : 0.5)),
                    child: Image.memory(
                      widget.images[position].bytes,
                      width: displayHeight(context)/6,
                      height: displayHeight(context)/6,
                    )),
              );
            }),
      ),
    );
  }
}
