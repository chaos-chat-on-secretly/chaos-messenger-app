import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageViewer extends StatelessWidget{

  final String imageUrl;

  ImageViewer(this.imageUrl);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.black,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        leading: BackButton(
          color: Colors.white,
        ),
        brightness: Brightness.dark,
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.transparent,
        bottomOpacity: 0.0,
      ),
      body: InteractiveViewer(
        child: SafeArea(
          child: Center(
            child: Image.network(
                imageUrl,
                height: double.infinity,
                width: double.infinity,
                fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );

  }

}