import 'package:animations/animations.dart';
import 'package:chaos/Screens/Chat/MediaViewers/Image.dart';
import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageMessageBuilder extends StatelessWidget {
  final ChatMessage message;
  final bool isCurrentUser;

  ImageMessageBuilder(this.message, this.isCurrentUser);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2, top: 2),
      child: FractionallySizedBox(
        alignment: isCurrentUser ? Alignment.centerRight : Alignment.centerLeft,
        widthFactor: 0.7,
        child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context).highlightColor,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  if (!isCurrentUser)
                    Padding(
                      padding: EdgeInsets.only(left: 5, top: 5),
                      child: Text(
                        message.user.name,
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .fontSize),
                      ),
                    ),
                  if (!isCurrentUser) SizedBox(height: 3),
                  OpenContainer(
                    openElevation: 0,
                    closedColor: Colors.transparent,
                    closedElevation: 0,
                    openBuilder: (_, action) {
                      return ImageViewer(message.image);
                    },
                    closedBuilder: (_, action) {
                      return Center(
                        child: ClipRRect(
                          borderRadius: isCurrentUser
                              ? BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))
                              : BorderRadius.all(Radius.zero),
                          child: Container(
                            constraints: BoxConstraints(
                              maxHeight: displayHeight(context) * 0.6
                            ),
                            child: Image.network(
                              message.image,
                              loadingBuilder: (BuildContext context,
                                  Widget child,
                                  ImageChunkEvent loadingProgress) {
                                if (loadingProgress == null) return child;
                                return Center(
                                  child: CircularProgressIndicator(
                                    value: loadingProgress.expectedTotalBytes !=
                                        null
                                        ? loadingProgress
                                        .cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes
                                        : null,
                                  ),
                                );
                              },
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      );
                    }
                  ),
                  SizedBox(height: 3),
                  Padding(
                    padding: EdgeInsets.only(right: 5, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "${DateFormat('hh:mm a').format(message.createdAt)}",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        SizedBox(width: 3),
                        if (isCurrentUser)
                          message.id.startsWith(
                                  "\$") //Checks if message is sent
                              ? Icon(
                                  Icons.check_circle,
                                  size: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .fontSize,
                                  color: Colors.green,
                                )
                              : Icon(
                                  Icons.access_time_rounded,
                                  size: Theme.of(context)
                                      .textTheme
                                      .caption
                                      .fontSize,
                                )
                      ],
                    ),
                  )
                ])),
      ),
    );
  }
}
