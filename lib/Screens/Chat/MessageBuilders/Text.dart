import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextMessageBuilder extends StatelessWidget{

  final ChatMessage message;
  final bool isCurrentUser;

  TextMessageBuilder(this.message, this.isCurrentUser);

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.only(bottom: 2, top: 2),
      child: FractionallySizedBox(
          alignment:
          isCurrentUser ? Alignment.centerRight : Alignment.centerLeft,
          widthFactor: 0.7,
          child: Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Theme.of(context).highlightColor,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    if (!isCurrentUser)
                      Text(
                        message.user.name,
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize:
                            Theme.of(context).textTheme.bodyText1.fontSize),
                      ),
                    if (!isCurrentUser) SizedBox(height: 3),
                    SelectableText(
                      message.text?.trim() ?? "Empty",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    SizedBox(height: 2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "${DateFormat('hh:mm a').format(message.createdAt)}",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        SizedBox(width: 3),
                        isCurrentUser
                            ? message.id.startsWith(
                            "\$") //Checks if message is sent
                            ? Icon(
                          Icons.check_circle,
                          size: Theme.of(context)
                              .textTheme
                              .caption
                              .fontSize,
                          color: Colors.green,
                        )
                            : Icon(
                          Icons.access_time_rounded,
                          size: Theme.of(context)
                              .textTheme
                              .caption
                              .fontSize,
                        )
                            : Container()
                      ],
                    )
                  ]))),
    );
  }

}