import 'package:animations/animations.dart';
import 'package:chaos/Screens/Chat/MediaViewers/Video.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class VideoMessageBuilder extends StatelessWidget {
  final ChatMessage message;
  final bool isCurrentUser;

  VideoMessageBuilder(this.message, this.isCurrentUser);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2, top: 2),
      child: FractionallySizedBox(
        alignment: isCurrentUser ? Alignment.centerRight : Alignment.centerLeft,
        widthFactor: 0.7,
        child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context).highlightColor,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  if (!isCurrentUser)
                    Padding(
                      padding: EdgeInsets.only(left: 5, top: 5),
                      child: Text(
                        message.user.name,
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .fontSize),
                      ),
                    ),
                  if (!isCurrentUser) SizedBox(height: 3),
                  OpenContainer(
                      openElevation: 0,
                      closedColor: Colors.transparent,
                      closedElevation: 0,
                      openBuilder: (_, action) {
                        return VideoViewer(message.video);
                      },
                      closedBuilder: (_, action) {
                        return Padding(
                          padding: EdgeInsets.all(25),
                          child: Center(
                            child: Column(
                              children: [
                                Icon(Icons.play_circle_filled, size: 50),
                              ],
                            ),
                          ),
                        );
                      }
                  ),
                  SizedBox(height: 3),
                  Padding(
                    padding: EdgeInsets.only(right: 5, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "${DateFormat('hh:mm a').format(message.createdAt)}",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        SizedBox(width: 3),
                        if (isCurrentUser)
                          message.id.startsWith(
                              "\$") //Checks if message is sent
                              ? Icon(
                            Icons.check_circle,
                            size: Theme.of(context)
                                .textTheme
                                .caption
                                .fontSize,
                            color: Colors.green,
                          )
                              : Icon(
                            Icons.access_time_rounded,
                            size: Theme.of(context)
                                .textTheme
                                .caption
                                .fontSize,
                          )
                      ],
                    ),
                  )
                ])),
      ),
    );
  }
}
