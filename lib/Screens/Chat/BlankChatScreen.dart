import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class BlankChatScreen extends StatefulWidget{

  @override
  __BlankChatScreenState createState() => __BlankChatScreenState();
}

class __BlankChatScreenState extends State<BlankChatScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text(
            'Select a contact to start chatting.',
            style: Theme.of(context).textTheme.headline5,
            textAlign: TextAlign.center,
          ),
        )
    );
  }
}

