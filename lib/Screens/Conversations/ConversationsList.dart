import 'package:chaos/Bloc/SignOutBloc.dart';
import 'package:chaos/Models/UI/ContextMenuItem.dart';
import 'package:chaos/Screens/Conversations/ConversationListItem.dart';
import 'package:chaos/Utilities/UI/StreamLoader.dart';
import 'package:chaos/main.dart';
import 'package:flutter/material.dart';
import 'package:chaos/Screens/SettingsScreen.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../AddParticipantsScreen.dart';
import '../CreateNewRoomScreen.dart';
import '../InviteDirectChatScreen.dart';

typedef Null ItemSelectedCallback(int value);

class ConversationList extends StatefulWidget {
  final int count;
  final ItemSelectedCallback onItemSelected;

  ConversationList(
      this.count,
      this.onItemSelected,
      );

  @override
  _ConversationListState createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> {

  List<ContextMenuItem> choices = const <ContextMenuItem>[
    const ContextMenuItem(title: 'Settings'),
    const ContextMenuItem(title: 'Sign Out'),
  ];

  void _select(ContextMenuItem choice) async {

    switch(choice.title){
      case "Sign Out":

        final signOutService = SignOutBloc();
        signOutService.signOut();

        await Navigator.of(context).push(
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    StreamLoader(
                      stream: signOutService.onNewStatus,
                    )
            )
        );

        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
              builder: (BuildContext context) => HomePage()
          ),
        );

        break;
      case "Settings":
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (BuildContext context) => SettingsScreen()
          ),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        actionsIconTheme: Theme.of(context).iconTheme,
        textTheme: Theme.of(context).textTheme,
        title: Text("Conversations"),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          PopupMenuButton<ContextMenuItem>(
            onSelected: _select,
            icon: Icon(Icons.more_vert ),
            itemBuilder: (BuildContext context) {
              return choices.map((ContextMenuItem choice) {
                return PopupMenuItem<ContextMenuItem>(
                  value: choice,
                  child: Text(choice.title,style:Theme.of(context).textTheme.bodyText2),
                );
              }).toList();
            },
          ),
        ],

      ),
      body: Column(
        children: [
          StreamBuilder(
              stream: ServicesWrapper.of(context).syncService.newConnectionState,
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {

                Widget banner=Container();

                if(snapshot.hasError){
                  banner = MaterialBanner(
                    backgroundColor: Theme.of(context).colorScheme.error,
                    contentTextStyle: TextStyle(color: Theme.of(context).colorScheme.onError),
                    content: Text(snapshot.error),
                    actions: [
                      TextButton(
                        child: const Text('Retry'),
                        onPressed: () {
                          // TODO: Implement retry button in banner

                          Fluttertoast.showToast(msg: "TODO");

                        },
                      ),
                    ],
                  );
                }

                return banner;
              }
          ),
          Expanded(
            child: ListView.separated(
              separatorBuilder: (_, __) => Divider(
                color: Theme.of(context).dividerColor,
                height: 0.5,
                thickness: 0.5,
              ),
              itemCount: widget.count,
              itemBuilder: (context, position) {
                return ConversationListItem(position, widget.onItemSelected);
              },
            ),
          ),
        ],
      ),


      floatingActionButton: FloatingActionButton(
        tooltip: "New conversation",
        child: Icon(Icons.add),
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return SafeArea(
                  child: Wrap(
                    children: <Widget>[
                      ListTile(
                          leading: Icon(Icons.chat),
                          title: Text('Direct Chat'),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext inviteDirectContext) => InviteDirectChatScreen(is_direct: true)
                              ) ,
                            );
                          }
                      ),
                      ListTile(
                          leading: Icon(Icons.group_add),
                          title: Text('New Room'),
                          onTap: () async {

                            List<String> members = await Navigator.of(context).push(
                                MaterialPageRoute<List<String>>(
                                    builder: (BuildContext addParticipantContext) => AddParticipantsScreen()
                                )
                            );

                            if(members != null){
                              await Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext createRoomContext) =>
                                          CreateNewRoomScreen(inviteList: members)
                                  )
                              );
                            }

                            Navigator.pop(context);

                          }
                      ),
                    ],
                  ),
                );
              }
          );
        },
      ),
    );
  }
}



