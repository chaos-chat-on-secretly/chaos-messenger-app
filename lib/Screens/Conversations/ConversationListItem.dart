import 'package:animations/animations.dart';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:chaos/Screens/Conversations/ConversationsList.dart';
import 'package:chaos/Screens/Profile/DirectChatProfile.dart';
import 'package:chaos/Screens/Profile/RoomProfile.dart';
import 'package:chaos/Utilities/UI/CircularAvatar.dart';
import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ConversationListItem extends StatelessWidget {
  final int position;
  final ItemSelectedCallback onItemSelected;

  ConversationListItem(this.position, this.onItemSelected);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<RoomListModel>(
        builder: (context, _, roomListModel) {
      List<RoomV2> roomListV2 = roomListModel.roomList;
      RoomV2 room = roomListV2[position];

      return ListTileTheme(
        selectedTileColor: Theme.of(context).highlightColor,
        selectedColor: Theme.of(context).colorScheme.onSurface,
        child: ListTile(
          selected: roomListModel.activeChat?.room_id == room.room_id,
          onTap: () => onItemSelected(position),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          isThreeLine: false,
          leading: isWeb()
              ? CircularAvatar(
                  text: room.room_name,
                  imageUrl: room.room_avatar,
                  radius: 25,
                )
              : OpenContainer(
                  closedColor: Colors.transparent,
                  closedElevation: 0,
                  closedBuilder: (_, action) {
                    return CircularAvatar(
                      text: room.room_name,
                      imageUrl: room.room_avatar,
                      radius: 25,
                    );
                  },
                  openBuilder: (BuildContext context,
                      void Function({Object returnValue}) action) {
                    return room.is_direct
                        ? DirectChatProfile(room)
                        : RoomProfile(room);
                  },
                ),
          title: Text(
            roomListV2[position].room_name,
            maxLines: 1,
          ),
          subtitle: Text(
            room.draftPresent ? "Draft: ${room.draft}" : room.lastEvent,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
      );
    });
  }
}
