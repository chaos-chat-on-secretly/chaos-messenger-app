import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Scoped%20Models/Chat.dart';
import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:flutter/material.dart';
import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:chaos/Screens/Chat/ChatScreen.dart';
import 'package:chaos/Screens/Conversations/ConversationsList.dart';
import 'package:scoped_model/scoped_model.dart';

class ConversationsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Row(children: <Widget>[
        Expanded(
          child: ScopedModelDescendant<RoomListModel>(
            builder: (context, _, roomListModel){

              // List<Room> roomList = roomListModel.roomList;
              List<RoomV2> roomListV2 = roomListModel.roomList;

              return ConversationList(roomListV2.length, (value) {
                if (roomListModel.activeChat?.room_id != roomListV2[value].room_id) {
                  roomListModel.activeChat = ChatModel(roomListV2[value]);
                }
                if (isMobile(context)) {
                  Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) =>
                            ScopedModel(
                                model: roomListModel.activeChat,
                                child: ChatScreen(isWeb: false)
                            ),
                      )
                  );
                }
              });
            },
          ),
          flex: 2,
        ),
        !isMobile(context)
            ?
        VerticalDivider(
          color: Theme.of(context).dividerColor,
          thickness: 0.5,
          width: 0.5,
        ) : Container(),
        !isMobile(context) ?
        Expanded(
          child: ScopedModelDescendant<RoomListModel>(
              builder: (context, _, roomListModel){
                return ScopedModel(
                    model: roomListModel.activeChat,
                    child: ChatScreen(isWeb:true)
                );
              }
          ),
          flex: 5,
        ) : Container(),
      ]),
    );
  }
}