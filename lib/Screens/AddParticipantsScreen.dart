import 'package:chaos/Utilities/Validation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AddParticipantsScreen extends StatefulWidget {

  final List<dynamic> join_members = new List.empty(growable: true);

  @override
  _AddParticipantsScreenState createState() => _AddParticipantsScreenState();
}

class _AddParticipantsScreenState extends State<AddParticipantsScreen> {
  String user_id;

  final userHolder = TextEditingController();

  List<String> members = List.empty(growable: true);

  _clearTextInput(){
    userHolder.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actionsIconTheme: Theme.of(context).iconTheme,
          textTheme: Theme.of(context).textTheme,
          title: Text("Add Participants"),
          leading: BackButton(),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.check),
                tooltip: MaterialLocalizations.of(context).okButtonLabel,
                onPressed: (){
                  Navigator.pop(context, members);
                }
            )
          ],
        ),
        body: Container(
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child:TextFormField(
                          controller: userHolder,
                          keyboardType: TextInputType.text,
                          cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                          decoration: InputDecoration(
                              labelText: 'Matrix Id',
                              border: OutlineInputBorder()
                          ),
                          onChanged: (value) {
                            user_id=value;
                          }
                      ),
                    ),
                    SizedBox(height: 15),
                    Center(
                        child: ElevatedButton(
                          child: Text('Add Participant'),
                          onPressed: () async {

                            if (user_id != null) {
                              if (validatorUserId(user_id)) {
                                _clearTextInput();
                                if(!members.contains(user_id)) {
                                  setState(() {
                                    members.add(user_id);
                                  });
                                }else{
                                  Fluttertoast.showToast(msg: "User already added");
                                }
                              }else{
                                Fluttertoast.showToast(msg: "Invalid user id");
                              }
                            }else{
                              Fluttertoast.showToast(msg: "Invalid user id");
                            }
                          }, //callback when button is clicked
                        )
                    ),
                    SizedBox(height: 15),
                    Divider(
                        color: Theme.of(context).dividerColor,
                        thickness:2
                    ),
                    SizedBox(height: 15),
                    members.length == 0 ?
                    Center(child: Text("No participants added"))
                        :
                    SizedBox(
                      height: 30,
                      child: ListView.separated(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: members.length,
                          itemBuilder:  (context, position){
                            return InputChip(
                                label: Text(members[position]),
                                useDeleteButtonTooltip: true,
                                deleteButtonTooltipMessage: "Remove user",
                                deleteIcon: Icon(Icons.close),
                                onDeleted: (){
                                  setState(() {
                                    members.remove(members[position]);
                                  });
                                }
                            );
                          }, separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(width: 2);
                      }),
                    )
                  ]
              ),
            )
        )
    );
  }
}