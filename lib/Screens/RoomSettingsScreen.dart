import 'package:flutter/material.dart';

class RoomSettings extends StatefulWidget {
  final String room_name;
  final String room_id;
  RoomSettings({this.room_name,this.room_id});
  @override
  _RoomSettingsState createState() => _RoomSettingsState();
}

class _RoomSettingsState extends State<RoomSettings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Room Settings"),
          actionsIconTheme: Theme.of(context).iconTheme,
          textTheme: Theme.of(context).textTheme,
          //title: Text("Settings"),
          leading: BackButton(),
          automaticallyImplyLeading: false,
        ),
        body:Container(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child:TextFormField(
                      keyboardType: TextInputType.text,
                      cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                      decoration: InputDecoration(
                          labelText: 'Room Name',
                          border: OutlineInputBorder()
                      ),
                      initialValue: widget.room_name ?? "",
                      onChanged: (value) {
                      }
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child:TextFormField(
                      keyboardType: TextInputType.text,
                      cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                      decoration: InputDecoration(
                          labelText: 'Description',
                          border: OutlineInputBorder()
                      ),
                      onChanged: (value) {
                      }
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Center(
                            child: ElevatedButton(
                              child: Text('Save'),
                              onPressed: () {

                              }, //callback when button is clicked
                            )
                        ),

                )]
            ))
    );
  }
}