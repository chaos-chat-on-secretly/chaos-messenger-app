import 'package:flutter/material.dart';
import 'package:chaos/Screens/Authentication/SignInScreen.dart';
import 'package:chaos/Screens/Authentication/SignUpScreen.dart';
import 'package:chaos/Screens/Authentication/AdvanceScreen.dart';


class AuthenticationScreen extends StatefulWidget {
  final String title;
  const AuthenticationScreen({@required this.title});
  @override
  AuthenticationScreenState createState() => AuthenticationScreenState();
}

class AuthenticationScreenState extends State<AuthenticationScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          actionsIconTheme: Theme.of(context).iconTheme,
          textTheme: Theme.of(context).textTheme,
          title: Text(widget.title),
          leading: BackButton(),
          automaticallyImplyLeading: false,
          bottom: TabBar(
            indicatorColor: Theme.of(context).accentColor,
            labelColor: Theme.of(context).primaryColorDark,
            tabs: [
              Tab(text: 'DETAILS'),
              Tab(text: 'ADVANCED'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            widget.title=="Sign Up"?SignUpScreen():SignInScreen(),
            AdvanceScreen()
          ],
        ),
      ),
    );

  }
}