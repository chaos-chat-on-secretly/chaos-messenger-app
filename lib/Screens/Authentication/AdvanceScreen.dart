import 'package:chaos/Utilities/URLs.dart';
import 'package:chaos/Utilities/Validation.dart';
import 'package:flutter/material.dart';

class AdvanceScreen extends StatefulWidget {

  @override
  _AdvanceScreenState createState() => _AdvanceScreenState();
}

class _AdvanceScreenState extends State<AdvanceScreen> {
  TextEditingController _serverFieldController = TextEditingController(text: serverUrl());
  bool serverUrlChanged;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    serverUrlChanged = false;
    super.initState();
  }

  serverFieldChange(String value){
    setState(() {
      if(serverUrl() == value) {
        serverUrlChanged = false;
      }else{
        serverUrlChanged = true;
      }
    });
  }

  void dispose() {
    _serverFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: SingleChildScrollView(
                child: Form(
                  key:_formKey,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 30),
                        Padding(
                            padding: const EdgeInsets.only( left: 20, right: 20),
                            child: Text(
                              'Only change, if you know what are you doing.',
                              style: Theme.of(context).textTheme.headline6,
                            )
                        ),
                        SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: TextFormField(
                              keyboardType: TextInputType.text,
                              controller: _serverFieldController,
                              cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              validator: validatorServerURL,
                              decoration: InputDecoration(
                                  labelText: 'Server URL',
                                  border: OutlineInputBorder()
                              ),
                              onChanged: serverFieldChange
                          ),
                        ),
                        SizedBox(height: 30),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Center(
                                  child: OutlinedButton(
                                    child: Text('Default'),
                                    onPressed: () {
                                      _serverFieldController.value = _serverFieldController.value.copyWith(text: defaultServer);
                                      serverFieldChange(_serverFieldController.text);
                                    }, //callback when button is clicked
                                  )
                              ),
                              SizedBox(width: 10),
                              Center(
                                  child: ElevatedButton(
                                    child: Text('Save'),
                                    onPressed: serverUrlChanged && _formKey.currentState.validate() ? () {
                                      var url = _serverFieldController.text;
                                      if (url[url.length - 1] == "/") {
                                        url = url.substring(0 , url.length - 1);
                                      }
                                      setServerUrl(url);
                                      setState(() {
                                        serverUrlChanged = false;
                                      });
                                    } : null, //callback when button is clicked
                                  )
                              ),
                            ]
                        )
                      ]
                  ),
                )
            )
        )
    );
  }
}