import 'package:flutter/material.dart';
import 'package:chaos/Screens/Conversations/ConversationsScreen.dart';
import 'package:flutter/services.dart';

class SignUpScreen extends StatefulWidget {

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String username;
  String password;
  String confirm;
  String email;
  bool obscurePass = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 30),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child:TextFormField(
                          cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                          autocorrect: false,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              labelText: 'Username',
                              border: OutlineInputBorder()
                            //hintText: 'Write here'
                          ),
                          //onSaved: (data) => name = data,
                          onChanged: (value) {
                            setState((){
                              username=value;
                            });
                          }

                      ),
                    ),
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child:TextFormField(
                          cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                          obscureText: obscurePass,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              labelText: 'Password',
                              border: OutlineInputBorder(),
                              suffixIcon: IconButton(
                                color: Theme.of(context).primaryColorDark,
                                icon: obscurePass ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
                                onPressed: (){
                                  setState(() {
                                    obscurePass = !obscurePass;
                                  });
                                },
                              )
                            //hintText: 'Write here',
                          ),

                          //onSaved: (data) => name = data,
                          onChanged: (value) {
                            setState((){
                              password=value;
                            });
                          }

                      ),
                    ),
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child:TextFormField(
                          cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                          obscureText: obscurePass,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              labelText: 'Repeat Password',
                              border: OutlineInputBorder(),
                              suffixIcon: IconButton(
                                color: Theme.of(context).primaryColorDark,
                                icon: obscurePass ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
                                onPressed: (){
                                  setState(() {
                                    obscurePass = !obscurePass;
                                  });
                                },
                              )
                            //hintText: 'Write here',
                          ),

                          //onSaved: (data) => name = data,
                          onChanged: (value) {
                            setState((){
                              confirm=value;
                            });
                          }

                      ),
                    ),
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child:TextFormField(
                          cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              labelText: 'Email (optional)',
                              border: OutlineInputBorder()
                            //hintText: 'Write here',
                          ),

                          //onSaved: (data) => name = data,
                          onChanged: (value) {
                            setState((){
                              email=value;
                            });
                          }

                      ),
                    ),
                    SizedBox(height: 30),
                    Center(
                        child: ElevatedButton(
                          child: Text('Sign Up'),
                          onPressed: () async {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ConversationsScreen()
                              ) ,
                            );
                          }, //callback when button is clicked
                        )
                    )
                  ]
              ),
            )
        )

    );
  }
}