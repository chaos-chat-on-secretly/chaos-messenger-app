import 'package:chaos/Bloc/InitialSyncBloc.dart';
import 'package:chaos/Bloc/SignInBloc.dart';
import 'package:chaos/Utilities/UI/StreamLoader.dart';
import 'package:chaos/Utilities/Validation.dart';
import 'package:chaos/main.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  String username;
  String password;

  bool obscurePass = true;

  final signInService = SignInBloc();

  final _formKey = GlobalKey<FormState>();


  bool isFormValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  onChanged: (){
                    isFormValid = _formKey.currentState.validate();
                  },
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: TextFormField(
                              cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                              autocorrect: false,
                              keyboardType: TextInputType.text,
                              validator: validatorUsername,
                              decoration: InputDecoration(
                                labelText: 'Username',
                                border: OutlineInputBorder(),
                              ),
                              //onSaved: (data) => name = data,
                              onChanged: (value) {
                                setState(() {
                                  username = value;
                                });

                              }),
                        ),
                        SizedBox(height: 15),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: TextFormField(
                              cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                              keyboardType: TextInputType.text,
                              obscureText: obscurePass,
                              validator:(value){
                                if(value.isEmpty){
                                  return 'Please enter password';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  labelText: 'Password',
                                  border: OutlineInputBorder(),
                                  suffixIcon: IconButton(
                                    color: Theme.of(context).primaryColorDark,
                                    icon: obscurePass
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                    onPressed: () {
                                      setState(() {
                                        obscurePass = !obscurePass;
                                      });
                                    },
                                  )),
                              //onSaved: (data) => name = data,
                              onChanged: (value) {
                                setState(() {
                                  password = value;
                                });
                              }),
                        ),
                        SizedBox(height: 30),
                        Center(
                          child: StreamBuilder(
                              stream: signInService.onNewStatus,
                              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                                Widget widget;
                                if (snapshot.hasError) {
                                  widget = Column(
                                    children: [
                                      Icon(Icons.error),
                                      SizedBox(height: 15),
                                      Text(
                                          "${snapshot.error}",
                                          textAlign: TextAlign.center
                                      ),
                                      SizedBox(height: 20),
                                      ElevatedButton(
                                          child: Text("Retry"),
                                          onPressed: isFormValid ? () {
                                            signInService.signIn(username, password);
                                          } : null
                                      )
                                    ],
                                  );
                                } else {
                                  switch (snapshot.connectionState) {
                                    case ConnectionState.waiting:
                                      widget = ElevatedButton(
                                          child: Text("Sign In"),
                                          onPressed: isFormValid ? () {
                                            signInService.signIn(username, password);
                                          } : null
                                      );
                                      break;

                                    case ConnectionState.active:
                                      widget = Column(
                                        children: [
                                          CircularProgressIndicator(),
                                          SizedBox(height: 15),
                                          Text("${snapshot.data}...")
                                        ],
                                      );
                                      break;

                                    case ConnectionState.done:

                                      widget = Icon(Icons.done);

                                      Fluttertoast.showToast(msg: "Signed In!");

                                      Future.delayed(Duration(seconds: 1), (){
                                        postSignIn(context);
                                      });

                                      break;

                                    case ConnectionState.none:

                                      widget = Text("Something unexpected happened!");

                                      break;
                                  }
                                }
                                return widget;
                              }
                          ),
                        )
                      ]
                  ),
                ))
        )
    );
  }

  void postSignIn(BuildContext context) async {

    final initialSyncService = InitialSyncBloc();

    initialSyncService.initialSync();

    await Navigator.of(context).push(
        MaterialPageRoute(
            builder: (BuildContext context) =>
                StreamLoader(
                  stream: initialSyncService.onNewStatus,
                )
        )
    );

    Fluttertoast.showToast(msg: "Syncing finished!");

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (BuildContext context) => HomePage()
        ),
            (Route<dynamic> route) => false
    );

  }
}
