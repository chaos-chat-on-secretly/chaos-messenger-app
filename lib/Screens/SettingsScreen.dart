import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          actionsIconTheme: Theme.of(context).iconTheme,
          textTheme: Theme.of(context).textTheme,
          title: Text("Settings"),
          leading: BackButton(),
          automaticallyImplyLeading: false,
        )
    );
  }
}