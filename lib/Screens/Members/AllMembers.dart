import 'package:chaos/Models/UserProfile.dart';
import 'package:chaos/Utilities/API/Rooms/RoomMembers.dart';
import 'package:chaos/Utilities/UI/CircularAvatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AllMembers extends StatelessWidget{

  final String _room_id;

  AllMembers(this._room_id);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        textTheme: Theme.of(context).textTheme,
        title: Text("Members"),
      ),
      body: FutureBuilder<List<UserProfile>>(
        future: getJoinedMembersFromServer(_room_id),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error.toString());
            return Text("Error");
          } else {
            Widget list = Placeholder();
            print(snapshot.connectionState);
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                break;
              case ConnectionState.waiting:
                list = Center(
                  child: CircularProgressIndicator(),
                );
                break;
              case ConnectionState.active:
              // TODO: Handle this case.
                break;
              case ConnectionState.done:

                List<UserProfile> members = snapshot.data;

                list = ListView.separated(
                  separatorBuilder: (context, position){
                    return Divider(
                      color: Theme.of(context).dividerColor,
                      height: 0.5,
                      thickness: 0.5,
                    );
                  },
                  itemBuilder: (context, position) {

                    UserProfile member = members[position];

                    return ListTile(
                      leading: CircularAvatar(
                        text: member.displayname,
                        imageUrl: member.avatar_url,
                        radius: 25,
                      ),
                      title: Text(member.displayname),
                      subtitle: Text(member.user_id),
                    );
                  },
                  itemCount: members.length,
                );
                break;
            }
            return list;
          }
        }
      ),
    );

  }

}