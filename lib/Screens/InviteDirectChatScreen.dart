import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:chaos/Utilities/API/Rooms/CreateRoom.dart';
import 'package:chaos/Utilities/API/Rooms/RoomsInfo.dart';
import 'package:chaos/Utilities/Validation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:scoped_model/scoped_model.dart';


class InviteDirectChatScreen extends StatefulWidget {
  final String room_id;
  final bool is_direct;
  InviteDirectChatScreen({this.room_id,this.is_direct});
  @override
  _InviteDirectChatScreenState createState() => _InviteDirectChatScreenState();
}

class _InviteDirectChatScreenState extends State<InviteDirectChatScreen> {
  String user_id;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actionsIconTheme: Theme.of(context).iconTheme,
          textTheme: Theme.of(context).textTheme,
          title: Text("New Direct Chat"),
          leading: BackButton(),
          automaticallyImplyLeading: false,
        ),
        body: Container(
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: TextFormField(
                          keyboardType: TextInputType.text,
                          cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
                          decoration: InputDecoration(
                              labelText: 'Matrix id',
                              border: OutlineInputBorder()
                          ),
                          onChanged: (value) {
                            user_id=value;
                          }
                      ),
                    ),
                    SizedBox(height: 15),
                    Center(
                        child: ElevatedButton(
                          child: Text('Start Direct Chat'),
                          onPressed: () async {

                            if (validatorUserId(user_id)) {
                              String displayName= (await getUserProfile(user_id)).displayname;

                              String response = await createRoom(
                                  displayName,
                                  widget.is_direct,
                                  ["$user_id"],
                                  ScopedModel.of<RoomListModel>(context)
                              );

                              if( response==ROOM_CREATED_SUCCESS ) {
                                Navigator.pop(context);
                              }
                              else if(response==CREATE_UNKNOWN_ERROR){
                                Fluttertoast.showToast(msg: "Network error!");
                              }
                              else{
                                Fluttertoast.showToast(msg: response);
                              }
                            }
                          }, //callback when button is clicked
                        )
                    )
                  ]
              ),
            )
        )
    );
  }
}