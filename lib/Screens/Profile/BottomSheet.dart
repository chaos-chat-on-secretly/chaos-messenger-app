
import 'package:chaos/Utilities/API/Rooms/RoomActions.dart';
import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:chaos/Utilities/UI/ConfirmationDialog.dart';

import 'package:flutter/material.dart';

bottomSheet(name, user_id, room_id, context){
  return showModalBottomSheet(
      backgroundColor: !isMobile(context) ? Colors.transparent : null,
      context: context,
      builder: (BuildContext bc) {
        return Padding(
          padding: EdgeInsets.only(
              left: bottomSheetPadding(context),
              right: bottomSheetPadding(context)
          ),
          child: Container(
            color: isMobile(context) ? null : Theme.of(context).scaffoldBackgroundColor,
            child: Wrap(
              children: <Widget>[
                // ListTile(
                //     leading: Icon(
                //       Icons.chat,
                //       // color: primary,
                //     ),
                //     title: Text('Direct Message'),
                //     onTap: () async {
                //       String title="Start direct chat with $name?";
                //       actionOnDirectMessage(context,title,user_id);
                //     }),
                ListTile(
                    leading: Icon(
                      Icons.remove_circle,
                    ),
                    title: Text('Remove'),
                    onTap: () {
                      String title="Do you want to remove $name?";
                      actionOnRemove(context, title, user_id, room_id);
                    }
                ),
              ],
            ),
          ),
        );
      });
}
// actionOnDirectMessage(BuildContext context,String title,String user_id) async {
//
//   //Take Confirmation
//   bool confirm = await showConfirmationDialog(context, title);
//
//   if(confirm) {
//     List id = [];
//
//     RoomListModel roomListModel = ScopedModel.of<RoomListModel>(context);
//     List<RoomModel> roomList = roomListModel.roomList;
//
//     for (var i = 0; i < roomList.length; i++) {
//       id.add(roomList[i].user_id);
//       if (roomList[i].user_id == user_id) {
//         Navigator.push(context , MaterialPageRoute(
//           builder: (context) {
//             roomListModel.activeChat = ChatModel(roomList[i].room_id, roomList[i].room_name, roomList[i].is_direct);
//             return isMobile(context) ? ScopedModel<ChatModel>(
//                 model: roomListModel.activeChat,
//                 child: ChatScreen()
//             ) : ConversationsScreen();
//           } ,
//         ));
//       }
//     }
//     if (!id.contains(user_id)) {
//
//       String displayname = await getDisplayName(user_id);
//
//       String response = await createRoom(
//           displayname ,
//           true ,
//           ["$user_id"],
//           ScopedModel.of<RoomListModel>(context)
//       );
//
//       if (response == ROOM_CREATED_SUCCESS) {
//         for (var i = 0; i < roomList.length; i++) {
//           if (roomList[i].user_id == user_id) {
//             Navigator.push(context , MaterialPageRoute(
//               builder: (context) {
//                 ScopedModel.of<RoomListModel>(context).activeChat =
//                     ChatModel(roomList[i].room_id, roomList[i].room_name, roomList[i].is_direct);
//                 return isMobile(context)  ? ScopedModel<ChatModel>(
//                     model: roomListModel.activeChat,
//                     child: ChatScreen()
//                 ) : ConversationsScreen();
//               } ,
//             ));
//           }
//         }
//       }
//       else if (response == CREATE_UNKNOWN_ERROR) {
//         Fluttertoast.showToast(msg: "Network error!");
//       }
//       else {
//         Fluttertoast.showToast(msg: response);
//       }
//     }
//   }
// }

actionOnRemove(BuildContext context, String title, String user_id, String room_id) async {
  bool confirm = await showConfirmationDialog(context, title);

  if(confirm){
    String response=await kickUser(room_id, user_id);
    if(response==KICK_SUCCESS){
      Fluttertoast.showToast(msg: "Member Removed");
    }
    else if(response==KICK_UNKNOWN_ERROR){
      Fluttertoast.showToast(msg: "Network error!");
    }
    else{
      Fluttertoast.showToast(msg: response);
    }
  }
}