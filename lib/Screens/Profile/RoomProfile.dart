import 'package:chaos/Models/Hive/Members/MatrixUser.dart';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Scoped%20Models/Chat.dart';
import 'package:chaos/Scoped%20Models/RoomList.dart';
import 'package:chaos/Screens/Members/AllMembers.dart';
import 'package:chaos/Screens/RoomSettingsScreen.dart';
import 'package:chaos/Utilities/API/Rooms/RoomActions.dart';
import 'package:chaos/Utilities/UI/ConfirmationDialog.dart';
import 'package:chaos/Utilities/UI/CircularAvatar.dart';
import 'package:chaos/main.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scoped_model/scoped_model.dart';

class RoomProfile extends StatefulWidget {
  final RoomV2 room;

  const RoomProfile(this.room);

  @override
  _RoomProfileState createState() => _RoomProfileState();
}

class _RoomProfileState extends State<RoomProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actionsIconTheme: Theme.of(context).iconTheme,
        textTheme: Theme.of(context).textTheme,
        leading: BackButton(),
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            SizedBox(height: 12),
            Center(
                child: CircularAvatar(
                    text: widget.room.room_name,
                    imageUrl: widget.room.room_avatar,
                    radius: 60)),
            Padding(
              padding: EdgeInsets.only(top: 15, left: 15, right: 15),
              child: Center(
                  child: Text(
                widget.room.room_name,
                textAlign: TextAlign.center,
              )),
            ),
            Padding(
                padding: EdgeInsets.only(top: 15),
                child: Divider(
                  color: Theme.of(context).dividerColor,
                  thickness: 1,
                )),
            ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: [
                ListTile(
                  onTap: () {
                    Fluttertoast.showToast(msg: "TODO");
                  },
                  leading: Icon(Icons.notifications),
                  title: Text('Notifications'),
                  trailing: Icon(Icons.arrow_forward_outlined),
                ),
                ListTile(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RoomSettings(
                                  room_name: widget.room.room_name,
                                  room_id: widget.room.room_id,
                                )));
                  },
                  leading: Icon(Icons.settings),
                  title: Text("Room Settings"),
                  trailing: Icon(Icons.arrow_forward_outlined),
                ),
                ListTile(
                  leading: Icon(Icons.group),
                  title: Text("Room Members"),
                  trailing: Icon(Icons.arrow_forward_outlined),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                AllMembers(widget.room.room_id)));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.exit_to_app),
                  title: Text("Leave Room"),
                  onTap: () async {
                    //Take confirmation
                    bool confirm = await showConfirmationDialog(
                        context, "Do you want to leave the room?");

                    if (confirm) {
                      RoomListModel roomListModel =
                          ScopedModel.of<RoomListModel>(context);

                      String response =
                          await leaveRoom(widget.room.room_id, roomListModel);

                      if (response == LEAVE_SUCCESS) {
                        roomListModel.activeChat = ChatModel.empty();
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (BuildContext context) => HomePage()),
                            ModalRoute.withName("/"));
                      } else if (response == LEAVE_UNKNOWN_ERROR) {
                        Fluttertoast.showToast(msg: "Network error!");
                      } else {
                        Fluttertoast.showToast(msg: response);
                      }
                    }
                  },
                ),
              ],
            ),

            // Padding(
            //   padding: EdgeInsets.all(10),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: [
            //       Text(
            //         "${(widget.join_members.length)} Participants",
            //         style:Theme.of(context).textTheme.bodyText1,
            //       ),
            //       IconButton(
            //         onPressed: () async {
            //           List<String> members = await Navigator.of(context).push(
            //               MaterialPageRoute(
            //                   builder: (BuildContext context) => AddParticipantsScreen()
            //               )
            //           );
            //
            //           if (members != null) {
            //             bool invited = false;
            //
            //             for(String member in members){
            //               String response=await inviteUser(widget.room_id,member);
            //               if(response==USER_INVITED_SUCCESS ) {
            //                 invited=true;
            //               }
            //               else{
            //                 Fluttertoast.showToast(msg: response);
            //               }
            //             }
            //             if(invited) {
            //               Fluttertoast.showToast(msg: "Invited Successfully");
            //             }
            //           }
            //         },
            //         icon: Icon(Icons.group_add),),
            //     ],
            //   ),
            // ),

            // Column(
            //   children: [
            //     ListView.builder(
            //         shrinkWrap:true,
            //         physics: NeverScrollableScrollPhysics(),
            //         itemCount: _viewMore?widget.join_members.length:_visibleItems,
            //         itemBuilder:  (context, position){
            //           return InkWell(
            //               onTap: (){
            //                 if(widget.join_members[position].user_id!=user_id){
            //                   bottomSheet(
            //                       widget.join_members[position].display_name,
            //                       widget.join_members[position].user_id,
            //                       widget.room_id,
            //                       context
            //                   );
            //                 }
            //               },
            //               child: Padding(
            //                   padding: EdgeInsets.all(5),
            //                   child: Row(
            //                     children: [
            //                       CircularAvatar(
            //                           initials: widget.join_members[position].display_name.substring(0,1),
            //                           radius: 17
            //                       ),
            //                       SizedBox(width: 5),
            //                       Flexible(
            //                         child: Text(
            //                           widget.join_members[position].display_name,
            //                           style: Theme.of(context).textTheme.headline6,
            //                           overflow: TextOverflow.ellipsis,
            //                         ),
            //                       ),
            //                     ],
            //                   )
            //               )
            //           );
            //         }
            //     ),
            //     widget.join_members.length>3 ? InkWell(
            //       onTap: (){
            //         setState(() {
            //           _viewMore=!_viewMore;
            //         });
            //       },
            //       child: Padding(
            //           padding: EdgeInsets.all(10),
            //           child:
            //           Row(
            //             children: [
            //               Icon(!_viewMore ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up),
            //               SizedBox(width: 5),
            //               Text(
            //                 !_viewMore?"View More":"Show less",
            //                 style: Theme.of(context).textTheme.subtitle1,
            //                 overflow: TextOverflow.ellipsis,
            //               ),
            //             ],
            //           )
            //       ),
            //     ) : Container(),
            //   ],
            // ),
          ])),
    );
  }
}
