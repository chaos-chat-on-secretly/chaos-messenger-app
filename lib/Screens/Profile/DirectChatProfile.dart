import 'package:chaos/Models/Hive/Members/MatrixUser.dart';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Utilities/UI/CircularAvatar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DirectChatProfile extends StatefulWidget {
  final RoomV2 room;

  const DirectChatProfile(this.room);

  @override
  _DirectChatProfileState createState() => _DirectChatProfileState();
}

class _DirectChatProfileState extends State<DirectChatProfile> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actionsIconTheme: Theme.of(context).iconTheme,
          textTheme: Theme.of(context).textTheme,
          leading: BackButton(),
          automaticallyImplyLeading: false,
        ),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 12),
              Center(
                  child: CircularAvatar(
                      text: widget.room.room_name,
                      imageUrl: widget.room.room_avatar,
                      radius: 60)),
              Padding(
                padding: EdgeInsets.only(top: 15, left: 15, right: 15),
                child: Center(
                    child: Text(
                  widget.room.room_name,
                  textAlign: TextAlign.center,
                )),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 15),
                  child: Divider(
                    color: Theme.of(context).dividerColor,
                    thickness: 2,
                  )),
              ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ListTile(
                      onTap: () {
                        Fluttertoast.showToast(msg: "TODO");
                      },
                      leading: Icon(Icons.notifications),
                      title: Text('Notifications'),
                      trailing: Icon(Icons.arrow_forward_outlined),
                    ),
                    ListTile(
                      onTap: () {
                        Fluttertoast.showToast(msg: "TODO");
                      },
                      leading: Icon(Icons.block),
                      title: Text('Block User'),
                    )
                  ]),
            ]));
  }
}
