import 'package:chaos/Screens/Authentication/AuthenticationScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class WelcomeScreen extends StatefulWidget {

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          top: false,
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: Center(
                          child: Text(
                            'Chaos: Chat Message Platform',
                            style: Theme.of(context).textTheme.headline5,
                      )
                      ),
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left:7,right: 7),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Expanded(
                              child: ElevatedButton(
                                child: Text('Sign In'),
                                onPressed: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) => AuthenticationScreen(title: "Sign In")
                                    ),
                                  );
                                }, //callback when button is clicked
                              ),
                            ),
                            SizedBox(width: 5),
                            Expanded(
                              child: ElevatedButton(
                                child: Text('Sign Up'),
                                onPressed: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) => AuthenticationScreen(title: "Sign Up")
                                    ),
                                  );
                                }, //callback when button is clicked
                              ),
                            ),
                          ]
                      )
                  )
                ]
            ),
          ),
        )
    );
  }
}