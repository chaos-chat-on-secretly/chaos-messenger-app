import 'package:chaos/Screens/Authentication/AuthenticationScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class WelcomeScreenLarge extends StatefulWidget{

  @override
  _WelcomeScreenLargeState createState() => _WelcomeScreenLargeState();
}

class _WelcomeScreenLargeState extends State<WelcomeScreenLarge>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Chaos - CHAt On Secretly',
                  style: Theme.of(context).textTheme.headline4,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10,),
                Text(
                  "We don't want your data!",
                  style: Theme.of(context).textTheme.headline6,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      child: Text('Sign In'),
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (BuildContext context) => AuthenticationScreen(title: "Sign In")
                          ),
                        );
                      }, //callback when button is clicked
                    ),
                    SizedBox(width: 5),
                    ElevatedButton(
                      child: Text('Sign Up'),
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (BuildContext context) => AuthenticationScreen(title: "Sign Up")
                          ),
                        );
                      }, //callback when button is clicked
                    )
                  ],
                ),
              ]
          ),
        )
    );
  }
}

