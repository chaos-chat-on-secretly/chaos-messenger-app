import 'package:chaos/Bloc/PeriodicSyncBloc.dart';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Scoped%20Models/Chat.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';
import 'package:chaos/Utilities/API/Rooms/RoomsInfo.dart';
import 'package:flutter/foundation.dart';
import 'package:scoped_model/scoped_model.dart';

class RoomListModel extends Model {

  final PeriodicSyncBloc syncService;

  RoomListModel({
    @required this.syncService
  });

  //List of rooms
  List<RoomV2> _roomList = List.empty(growable: true);
  List<RoomV2> get roomList => _roomList;

  //Active chat
  ChatModel _activeChat = ChatModel.empty();
  ChatModel get activeChat => _activeChat;
  set activeChat(ChatModel chat) {
    _activeChat = chat;
    // Then notify all the listeners.
    notifyListeners();
  }

  void init() async {

    List<String> rooms = await roomListFromLocalStorage();

    _roomList.clear();

    for(String room_id in rooms){
      RoomV2 room = RoomV2(room_id);
      await room.load();
      if(!roomExists(room.room_id))
        _roomList.add(room);
    }

    assert(_roomList.length == rooms.length);

    notifyListeners();

    //Initialise stream listeners
    roomUpdatesListener();
  }


  //Room list updates
  void roomUpdatesListener(){

    syncService.newRoomsJoinEvent.listen((roomsJoinEvents) async {

      //Add room if absent
      for(String room_id in roomsJoinEvents.keys){
        await addRoomIfAbsent(room_id);
      }

      List<RoomV2> updatedRooms = (_roomList.where((room) => roomsJoinEvents.containsKey(room.room_id))).toList();

      if(updatedRooms.isNotEmpty){
        updatedRooms.forEach((room) async {

          List<dynamic> events = roomsJoinEvents[room.room_id]['timeline']['events'];

          if(events.isNotEmpty){

            room.updateRoomEvents(roomsJoinEvents[room.room_id])
                .then((value){
                  _activeChat.notifyListeners();

                  //Move the event to top
                  _roomList.removeWhere((element) => element.room_id == room.room_id);
                  _roomList.insert(0, room);

                  //Notify the changes in room list;
                  notifyListeners();

                  //Save to local storage
                  saveRoomList();
                });
          }
        });
      }

    });


  }

  //Checks if a room exists in the list
  bool roomExists(String room_id){
    for (RoomV2 room in _roomList) {
      if(room.room_id==room_id) return true;
    }
    return false;
  }

  //Adds room to the list
  void addRoom(RoomV2 room) {
    //Add room
    _roomList.insert(0, room);

    // Then notify all the listeners.
    notifyListeners();

    //Save to local storage
    saveRoomList();
  }

  //Add room if it is absent in the list
  Future<void> addRoomIfAbsent(String room_id) async {
    if(!roomExists(room_id)){

      //Get room object
      RoomV2 new_room = await getRoomModel(room_id);

      //Add it to the list
      addRoom(new_room);

      // Then notify all the listeners.
      notifyListeners();

      //Save to local storage
      saveRoomList();
    }
  }

  //Remove room from the list
  Future<void> removeRoom(String room_id) async {

    //Remove room
    _roomList.removeWhere((room) => room.room_id == room_id);

    // Then notify all the listeners.
    notifyListeners();

    //Save to local storage
    saveRoomList();
  }

  //Updates a particular room in the list
  void updateRoomData(RoomV2 room){

    //Update the room
    int index = _roomList.indexWhere((element) => element.room_id == room.room_id);
    _roomList.replaceRange(index, index+1, [room]);

    // Then notify all the listeners.
    notifyListeners();

    //Save to local storage
    saveRoomList();
  }

  void saveRoomList(){

    List<String> rooms = List<String>.empty(growable: true);

    for(RoomV2 room in _roomList){
      if(!rooms.contains(room.room_id))
        rooms.add(room.room_id);
    }

    roomListToLocalStorage(rooms);

  }
}