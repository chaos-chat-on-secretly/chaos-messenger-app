import 'dart:async';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';
import 'package:chaos/Utilities/API/Messages.dart';
import 'package:chaos/Utilities/API/SyncLocalStorage.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:scoped_model/scoped_model.dart';

class ChatModel extends Model {

  //If message is being sent
  bool _sendingEvent=false;

  //Room Id
  String get room_id => room?.room_id;

  //Room name
  String get room_name => room?.room_name;

  //If room is direct chat
  bool get is_direct => room?.is_direct;

  //Messages
  List<ChatMessage> get messages => room?.events ?? List.empty();

  //Message queue
  List<ChatMessage> _unSentEventsQueue = List<ChatMessage>.empty(growable: true);

  //V2
  RoomV2 room;

  ChatModel(this.room){
    Timer.periodic(Duration(milliseconds: 500), eventsSender);
  }

  ChatModel.empty();
  
  void clearMessages() {
    room.events = List<ChatMessage>.empty(growable: true);
    room.lastEvent = '';

    // Then notify all the listeners.
    notifyListeners();

    //Clear locally saved messages
    saveMessagesListToLocalStorage(room_id, messages);
  }

  void _addMessage(ChatMessage message){
    messages.add(message);

    // Then notify all the listeners.
    notifyListeners();

    //Save to local storage
    saveMessagesListToLocalStorage(room_id, messages);
  }

  void eventsSender(Timer timer) async {
    if(_unSentEventsQueue.isNotEmpty && !_sendingEvent){

      _sendingEvent = true;

      ChatMessage message = _unSentEventsQueue.first;

      String response = await sendEvent(room_id, message);

      if(response != SEND_MESSAGE_ERROR){

        _unSentEventsQueue.removeAt(0);

        handledEvents.add(response);

        ChatMessage unsentMessage = messages.singleWhere((element) => element.id == message.id);

        unsentMessage.id = response;

        notifyListeners();

        saveMessagesListToLocalStorage(room_id, messages);

      }

      _sendingEvent = false;

    }
  }

  void sendTextMessage(ChatMessage message) async {

    _addMessage(message);

    _unSentEventsQueue.add(message);

  }

  void loadOlderEvents() async {
    await room.loadOlderEvents();
    notifyListeners();
  }
}