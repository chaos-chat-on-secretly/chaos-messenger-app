import 'package:chaos/Bloc/PeriodicSyncBloc.dart';
import 'package:chaos/Screens/Conversations/ConversationsScreen.dart';
import 'package:chaos/Screens/Welcome/WelcomeScreen.dart';
import 'package:chaos/Screens/Welcome/WelcomeScreen_large.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:chaos/Utilities/UI/Sizes.dart';
import 'package:flutter/material.dart';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:scoped_model/scoped_model.dart';
import 'Utilities/UI/FullScreenLoader.dart';
import 'Utilities/UI/Themes.dart';

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:chaos/Scoped Models/RoomList.dart';

void main() async {
  await Hive.initFlutter();
  // Hive.registerAdapter(RoomAdapter());
  // Hive.registerAdapter(MatrixUserAdapter());
  runApp(ChaosApp());
}

class ChaosApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ServicesWrapper(
        child: ModelWrapper()
    );
  }
}

class ServicesWrapper extends InheritedWidget{

  final syncService = PeriodicSyncBloc();

  ServicesWrapper({
    Widget child
  }) : super(child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return false;
  }

  static ServicesWrapper of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ServicesWrapper>();
  }

}

class ModelWrapper extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ScopedModel<RoomListModel>(
        model: RoomListModel(
            syncService: ServicesWrapper.of(context).syncService
        ),
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: lightTheme,
            darkTheme: darkTheme,
            home: HomePage()
        )
    );
  }
  
}

class HomePage extends StatelessWidget {

  Future<bool> _onStart() async {
    await loadServerDetails(); // Initialize local data
    return await sessionExists();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _onStart(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            if(snapshot.data){

              //TODO: Check if next_batch is available, if not, do initialSync()

              //Load Saved Room Data
              ScopedModel.of<RoomListModel>(context).init();

              //Start sync service
              ServicesWrapper.of(context).syncService.start();

              return ConversationsScreen();
            }else{
              return isMobile(context) ? WelcomeScreen() : WelcomeScreenLarge();
            }
          } else if (snapshot.hasError) {
            return Center(child: Text("Error!"));
          } else {
            return FullScreenLoader(
              loadingText: "Initializing",
            );
          }
        }
    );
  }
}

