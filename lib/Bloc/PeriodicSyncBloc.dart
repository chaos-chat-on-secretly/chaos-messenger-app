import 'dart:async';
import 'dart:convert';

import 'package:chaos/Models/Sync/PeriodicSync.dart';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:chaos/Utilities/API/sharedPrefs.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:http/http.dart' as http;

class PeriodicSyncBloc{

  //Stream for network connectivity in syncing
  final StreamController _connectionController = StreamController<String>.broadcast();
  Stream<String> get newConnectionState => _connectionController.stream;

  //Stream for new joined room events
  final StreamController _roomsJoinController = StreamController<Map<String, dynamic>>();
  Stream<Map<String, dynamic>> get newRoomsJoinEvent => _roomsJoinController.stream;

  void start(){

    //Start Syncing
    _syncService();

  }


  void _syncService() async {

    String next_batch = await getStringSharedPrefs("next_batch");

    Timer.periodic(Duration(seconds: 3), (timer) async {

      String url = syncWithNextBatchUrl(next_batch);

      try {
        http.Response response = await http.get(Uri.parse(url), headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        });

        if(response.statusCode == 200){

          //Update sync connection status
          _connectionController.add("Sync Success");

          //Parse sync data
          PeriodicSyncModel periodicSyncModel = PeriodicSyncModel.fromJson(json.decode(response.body));

          //Save next batch
          next_batch = periodicSyncModel.next_batch;
          await saveNextBatch(periodicSyncModel.next_batch);

          //Stream Events
          _streamSyncData(periodicSyncModel);

        }else{
          print("PeriodicSync: ${response.body}");

          //Cancel the timer
          timer.cancel();

          _connectionController.addError(response.body);
        }
      } on Exception catch (e) {
        print("PeriodicSync: $e");
        _connectionController.addError("Trouble connecting to server.");
      }
    }
    );
  }

  void _streamSyncData(PeriodicSyncModel periodicSyncData) {

    _roomsJoinController.add(periodicSyncData.roomsJoin);

  }

}