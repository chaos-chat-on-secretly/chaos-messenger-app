import 'dart:async';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:http/http.dart' as http;

class SignOutBloc {
  final StreamController _controller = StreamController<String>();
  Stream<String> get onNewStatus => _controller.stream;

  void signOut() async {
    try {
      //Request server to sign out
      _controller.add("Requesting server");
      final http.Response response = await _signOut();

      if (response.statusCode == 200) {

        //Clear session
        _controller.add("Clearing session");
        clearSession();

        //Delete local data
        _controller.add("Clearing local data");
        await clearLocalData();

        //Done
        _controller.close();
      }else{
        _controller.addError(response.body);
      }
    } on Exception catch (e) {
      _controller.addError(e.toString());
    }
  }

  Future<http.Response> _signOut() async {
    String url = signOutUrl();
    return await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        }
    );
  }

}