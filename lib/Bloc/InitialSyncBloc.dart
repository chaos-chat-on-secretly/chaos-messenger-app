import 'dart:async';
import 'dart:convert';
import 'package:chaos/Models/FilterId.dart';
import 'package:chaos/Models/RoomV2.dart';
import 'package:chaos/Models/Sync/InitialSync.dart';
import 'package:chaos/Models/UserProfile.dart';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:chaos/Utilities/API/Events.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';
import 'package:chaos/Utilities/API/Rooms/RoomsInfo.dart';
import 'package:chaos/Utilities/API/SyncLocalStorage.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:http/http.dart' as http;

class InitialSyncBloc {
  final StreamController _controller = StreamController<String>();
  Stream<String> get onNewStatus => _controller.stream;

  void initialSync() async {

    try {

      //Sync account data
      _controller.add("Gathering account info");
      InitialSyncModel initialSyncResponse = await _initialSync();

      //Save next batch
      saveNextBatch(initialSyncResponse.next_batch);

      //Enumerate rooms
      _controller.add("Enumerating rooms");
      List<String> roomListV2 = await getRoomListFromInitialSync(initialSyncResponse);

      //Save locally
      _controller.add("Saving");
      await roomListToLocalStorage(roomListV2);

      //Done
      _controller.close();

    } on Exception catch (e) {

      //Error
      _controller.addError(e);

    }

  }

  Future<InitialSyncModel> _initialSync() async {

    //Generate Filter
    String filterId = await _generateFilter();

    //Sync with filter

    String syncUrl = syncWithFilterUrl(filterId);

    http.Response initialSyncResponse = await http.get(Uri.parse(syncUrl),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        }
    );

    if(initialSyncResponse.statusCode==200){
      return InitialSyncModel.fromJson(json.decode(initialSyncResponse.body));
    }else{
      throw Exception(initialSyncResponse.body);
    }

  }

  Future<List<String>> getRoomListFromInitialSync(InitialSyncModel initialSyncData) async {
    // List<Room> roomList = new List<Room>.empty(growable: true);
    List<String> roomListV2 = List<String>.empty(growable: true);

    for (String room_id in initialSyncData.roomsJoin.keys) {

      try {

        //Extract latest event
        String lastEvent = await getEventSummary(initialSyncData.roomsJoin[room_id]['timeline']['events'][0]);

        //get room info

        String room_name;
        String room_avatar;
        bool is_direct=false;

        room_name = await getRoomName(room_id);
        room_avatar = await getRoomAvatar(room_id);

        //Check if direct chat
        for (MapEntry<String, dynamic> direct in initialSyncData.directs.entries) {
          if(direct.value.contains(room_id)){
            is_direct=true;
            UserProfile directChatRoomMember = await getUserProfile(direct.key);
            room_name ??= directChatRoomMember?.displayname;
            room_avatar ??= directChatRoomMember?.avatar_url;
            break;
          }
        }

        //Initialise room

        RoomV2 room = RoomV2(room_id);

        //Add values
        room.room_name = room_name;
        room.room_avatar = room_avatar;
        room.is_direct = is_direct;
        room.lastEvent = lastEvent;
        room.updateRoomEvents(initialSyncData.roomsJoin[room_id]);
        room.savePrevBatch(initialSyncData.roomsJoin[room_id]['timeline']['prev_batch']);

        //Add to room list
        roomListV2.add(room_id);

      } on Exception catch (e) {
        print("getRoomListFromInitialSync: ${e.toString()}");
        continue;
      }

    }

    return roomListV2;
  }

  Future<String> _generateFilter() async {

    String url = filterUrl();

    Object filter = {
      "room": {
        "state": {
          "types": [],
          "not_rooms": []
        },
        "timeline": {
          "limit": 1,
        },
        "ephemeral": {
          "types": [],
          "not_rooms": [],
          "not_senders": []
        }
      },
      "presence": {
        "types": ["m.presence"],
        "not_senders": []
      },
      "event_format": "client",
      // "event_fields": ["type", "content", "sender", "event_id"]
    };

    http.Response filterIdResponse = await http.post(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "authorization": "Bearer $token"
        },
        body: json.encode(filter)
    );

    if (filterIdResponse.statusCode == 200) {
      return FilterIdModel.fromJson(json.decode(filterIdResponse.body)).filter_id;
    }else{
      throw Exception(filterIdResponse.body);
    }
  }

}