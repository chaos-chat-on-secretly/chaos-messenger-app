import 'dart:async';
import 'dart:convert';
import 'package:chaos/Models/AuthTypes/MLoginPassword.dart';
import 'package:chaos/Utilities/API/Authentication.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:http/http.dart' as http;

class SignInBloc {
  final StreamController _controller = StreamController<String>();

  Stream<String> get onNewStatus => _controller.stream;

  void signIn(String username, String password) async {

    try {

      // Determine authentication type
      _controller.add("Determining auth type");
      String authType = await getAuthenticationType();

      if (authType != UNKNOWN_ERROR &&
          authType != M_LIMIT_EXCEEDED) {

        // Sign in
        _controller.add("Signing in");
        Map<String, dynamic> response = await _signIn(username, password, authType);

        if (response['statusCode'] == 200) {

          // Save session
          await saveSession(response['data']);
          _controller.add("Saving session");

          // done
          _controller.close();


        } else {
          _controller.addError(response['error']);
        }

      } else {
        _controller.addError(authType);
      }

    } on Exception catch (e) {
      _controller.addError(e.toString());
    }

  }

  Future<Map<String, dynamic>> _signIn(String username, String password, String authType) async {

    Map<String, dynamic> response;

    switch (authType) {

      case M_LOGIN_PASSWORD:
        response = await _mLoginPassword(username, password);
        break;

      case M_LOGIN_DUMMY:
        response = await _mLoginPassword(username, password);
        break;

      case UNSUPPORTED_LOGIN_TYPE:
        response = {
          'statusCode': 400,
          'error': UNSUPPORTED_LOGIN_TYPE
        };

    }
    return response;
  }

  //login type handlers

  //m.login.password
  Future<Map<String, dynamic>> _mLoginPassword(String username, String password) async {

    Object data = {
      "type": M_LOGIN_PASSWORD,
      "user": username,
      "password": password
    };

    http.Response response = await http.post(
        Uri.parse(signInUrl()),
        headers: {
          "Content-Type": "application/json"
        },
        body: json.encode(data)
    );

    Map<String, dynamic> result = Map<String, dynamic>();

    result['statusCode'] = response.statusCode;

    switch(response.statusCode){

      case 200:
        result['data'] = MLoginPasswordModel.fromJson(json.decode(response.body));
        break;
      case 403:
        result['error'] = json.decode(response.body)['error'];
        break;
    }

    return result;

  }
}