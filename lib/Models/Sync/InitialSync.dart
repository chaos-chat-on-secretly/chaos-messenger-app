class InitialSyncModel {
  final roomsJoin;
  final roomsInvite;
  final roomsLeave;
  final Map<String, dynamic> directs;
  final next_batch;

  InitialSyncModel({ this.roomsJoin, this.roomsInvite, this.roomsLeave, this.directs, this.next_batch});

  factory InitialSyncModel.fromJson(Map<String, dynamic> json) {
    // for extracting direct rooms
    Map<String, dynamic> directs;
    for(final events in json['account_data']["events"]){
      if(events['type']=="m.direct"){
        directs=events['content'];
        break;
      }
    }
    directs ??= Map<String, dynamic>();
    return InitialSyncModel(
      roomsJoin: json['rooms']["join"],
      roomsInvite: json['rooms']['invite'],
      roomsLeave: json['rooms']['leave'],
      directs: directs,
      next_batch: json['next_batch']
    );
  }
}