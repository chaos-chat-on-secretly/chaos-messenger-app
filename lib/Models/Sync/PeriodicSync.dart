class PeriodicSyncModel {
  final Map<String, dynamic> roomsJoin;
  final Map<String, dynamic> roomsInvite;
  final Map<String, dynamic> roomsLeave;
  final String next_batch;

  PeriodicSyncModel({ this.roomsJoin, this.roomsInvite, this.roomsLeave, this.next_batch});

  factory PeriodicSyncModel.fromJson(Map<String, dynamic> json) {
    return PeriodicSyncModel(
        roomsJoin: json['rooms']['join'],
        roomsInvite: json['rooms']['invite'],
        roomsLeave: json['rooms']['leave'],
        next_batch: json['next_batch']
    );
  }
}