class AccountDataModel {
  final Map<String, dynamic> content;

  AccountDataModel({this.content});

  factory AccountDataModel.fromJson(Map<String, dynamic> json) {
    return AccountDataModel(
      content: json,
    );
  }
}
