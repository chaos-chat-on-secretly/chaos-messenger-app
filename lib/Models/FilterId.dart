class FilterIdModel {
  final String filter_id;

  FilterIdModel({this.filter_id});

  factory FilterIdModel.fromJson(Map<String, dynamic> json) {
    return FilterIdModel(
      filter_id: json['filter_id'],
    );
  }
}
