import 'package:chaos/Models/UserProfile.dart';
import 'package:chaos/Utilities/API/Events.dart';
import 'package:chaos/Utilities/API/Rooms/RoomsInfo.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:flutter/material.dart';

class EventModel {
  final String type;
  final Map<String, dynamic> content;

  EventModel({ this.type, this.content });

  factory EventModel.fromJson(Map<String, dynamic> json) {
    return EventModel(
        type: json['type'],
        content: json['content']
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    try {
      data['type'] = this.type;
      data['content'] = this.content;
    } catch (e, stack) {
      print('ERROR caught when trying to convert Event to JSON:');
      print(e);
      print(stack);
    }
    return data;
  }
}

class RoomEventModel extends EventModel{
  final String event_id;
  final String sender;
  final int origin_server_ts;
  Map<String, dynamic> unsigned;

  RoomEventModel({
    @required String type,
    @required Map<String, dynamic> content,
    @required this.event_id,
    @required this.sender,
    @required this.origin_server_ts,
    this.unsigned
  }) : super(type: type, content: content);

  factory RoomEventModel.fromJson(Map<String, dynamic> json){
    return RoomEventModel(
        type: json['type'],
        content: json['content'],
        event_id: json['event_id'],
        sender: json['sender'],
        origin_server_ts: json['origin_server_ts'],
        unsigned: json['unsigned']
    );
  }

  Future<ChatMessage> toTextMessage() async {

    UserProfile user = await getUserProfile(sender);

    assert(user.runtimeType == UserProfile || user == null);

    return ChatMessage(
      createdAt: DateTime.fromMillisecondsSinceEpoch(origin_server_ts),
      id: event_id,
      eventType: type,
      user: ChatUser(
          uid: sender,
          name: user?.displayname,
          avatar: user?.avatar_url
      ),
      text: content['body'],
    );
  }

  Future<ChatMessage> toImageMessage() async {
    UserProfile user = await getUserProfile(sender);

    String imageUrl = mxcToHttpUrl(content['url']);

    assert(user.runtimeType == UserProfile || user == null);

    return ChatMessage(
      id: event_id,
      eventType: type,
      createdAt: DateTime.fromMillisecondsSinceEpoch(origin_server_ts),
      text: null,
      image: imageUrl,
      user: ChatUser(
          uid: sender,
          name: user?.displayname,
          avatar: user?.avatar_url
      ),
      // customProperties: Map.castFrom(content['info'])
    );
  }

  Future<ChatMessage> toVideoMessage() async {
    UserProfile user = await getUserProfile(sender);

    String videoUrl = mxcToHttpUrl(content['url']);

    assert(user.runtimeType == UserProfile || user == null);

    return ChatMessage(
      id: event_id,
      eventType: type,
      createdAt: DateTime.fromMillisecondsSinceEpoch(origin_server_ts),
      text: null,
      video: videoUrl,
      user: ChatUser(
          uid: sender,
          name: user?.displayname,
          avatar: user?.avatar_url
      ),
    );
  }

  Future<ChatMessage> toChatEvent() async {

    UserProfile user = await getUserProfile(sender);

    String text = await getEventSummary(this.toJson());

    assert(user.runtimeType == UserProfile || user == null);

    return ChatMessage(
      isInfoMessage: true,
      createdAt: DateTime.fromMillisecondsSinceEpoch(origin_server_ts),
      id: event_id,
      eventType: type,
      user: ChatUser(
          uid: sender,
          name: user?.displayname,
          avatar: user?.avatar_url
      ),
      text: text,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    try {
      data['type'] = this.type;
      data['content'] = this.content;
      data['event_id'] = this.event_id;
      data['sender']= this.sender;
      data['unsigned'] = this.unsigned;

    } catch (e, stack) {
      print('ERROR caught when trying to convert RoomEvent to JSON:');
      print(e);
      print(stack);
    }
    return data;
  }
}

class StateEventModel extends RoomEventModel{
  final String state_key;
  Map<String, dynamic> prev_content;

  StateEventModel({
    @required String type,
    @required Map<String, dynamic> content,
    @required String event_id,
    @required String sender,
    @required int origin_server_ts,
    Map<String, dynamic> unsigned,
    @required this.state_key,
    this.prev_content
  }) : super(type: type, content: content, event_id: event_id, sender: sender, origin_server_ts: origin_server_ts);

  factory StateEventModel.fromJson(Map<String, dynamic> json){
    return StateEventModel(
        type: json['type'],
        content: json['content'],
        event_id: json['event_id'],
        sender: json['sender'],
        origin_server_ts: json['origin_server_ts'],
        unsigned: json['unsigned'],
        state_key: json['state_key'],
        prev_content: json['prev_content']
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    try {
      data['type'] = this.type;
      data['content'] = this.content;
      data['event_id'] = this.event_id;
      data['sender']= this.sender;
      data['unsigned'] = this.unsigned;
      data['state_key'] = this.state_key;
      data['prev_content'] = this.prev_content;

    } catch (e, stack) {
      print('ERROR caught when trying to convert ChatMessage to JSON:');
      print(e);
      print(stack);
    }
    return data;
  }
}