class OlderRoomEvents {
  List<dynamic> events;
  String start;
  String end;
  OlderRoomEvents({ this.events, this.start, this.end });

  factory OlderRoomEvents.fromJson(Map<String, dynamic> json) {
    return OlderRoomEvents(
        events: json['chunk'],
        start: json['start'],
        end: json['end']
    );
  }
}