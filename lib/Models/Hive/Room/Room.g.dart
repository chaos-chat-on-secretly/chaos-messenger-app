// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Room.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RoomAdapter extends TypeAdapter<Room> {
  @override
  final int typeId = 0;

  @override
  Room read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Room(
      fields[0] as String,
      room_name: fields[1] as String,
      room_avatar: fields[2] as String,
      is_direct: fields[3] as bool,
      lastEvent: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Room obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.room_id)
      ..writeByte(1)
      ..write(obj.room_name)
      ..writeByte(2)
      ..write(obj.room_avatar)
      ..writeByte(3)
      ..write(obj.is_direct)
      ..writeByte(4)
      ..write(obj.lastEvent);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RoomAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
