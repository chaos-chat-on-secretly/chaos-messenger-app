import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

part 'Room.g.dart';

@HiveType(typeId: 0)
class Room {
  @HiveField(0)
  final String room_id;

  @HiveField(1)
  String room_name;

  @HiveField(2)
  String room_avatar;

  @HiveField(3)
  bool is_direct;

  @HiveField(4)
  String lastEvent;

  Room(this.room_id, {this.room_name, this.room_avatar, this.is_direct, this.lastEvent}){
    room_name ??= room_id;
    is_direct ??= false;
    lastEvent ??= "";
  }
}