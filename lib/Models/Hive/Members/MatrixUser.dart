import 'package:hive/hive.dart';

part 'MatrixUser.g.dart';
@HiveType(typeId: 1)
class MatrixUser {
  @HiveField(0)
  final String user_id;

  @HiveField(1)
  final String display_name;

  @HiveField(2)
  final String avatar_url;

  MatrixUser(this.user_id, this.display_name, this.avatar_url);
}