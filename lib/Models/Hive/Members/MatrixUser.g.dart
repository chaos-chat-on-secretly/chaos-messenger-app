// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MatrixUser.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MatrixUserAdapter extends TypeAdapter<MatrixUser> {
  @override
  final int typeId = 1;

  @override
  MatrixUser read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MatrixUser(
      fields[0] as String,
      fields[1] as String,
      fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, MatrixUser obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.user_id)
      ..writeByte(1)
      ..write(obj.display_name)
      ..writeByte(2)
      ..write(obj.avatar_url);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MatrixUserAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
