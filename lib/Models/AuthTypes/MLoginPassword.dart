class MLoginPasswordModel {
  final String user_id;
  final String access_token;
  final String home_server;
  final String device_id;
  final Object well_known;

  MLoginPasswordModel({this.user_id, this.access_token, this.home_server, this.device_id, this.well_known});

  factory MLoginPasswordModel.fromJson(Map<String, dynamic> json) {
    return MLoginPasswordModel(
      user_id: json['user_id'],
      access_token: json['access_token'],
      home_server: json['home_server'],
      device_id: json['device_id'],
      well_known: json['well_known'],
    );
  }
}
