class RoomNameModel {
  final String name;

  RoomNameModel({ this.name});

  factory RoomNameModel.fromJson(Map<String, dynamic> json) {
    return RoomNameModel(
      name: json['name'],
    );
  }
}

class RoomAvatarModel {
  final String url;

  RoomAvatarModel({ this.url});

  factory RoomAvatarModel.fromJson(Map<String, dynamic> json) {
    return RoomAvatarModel(
      url: json['url'],
    );
  }
}