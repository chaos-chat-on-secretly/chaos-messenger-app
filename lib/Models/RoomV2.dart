import 'package:chaos/Utilities/API/Events.dart';
import 'package:chaos/Utilities/API/Rooms/RoomsInfo.dart';
import 'package:chaos/Utilities/API/SyncLocalStorage.dart';
import 'package:chaos/Utilities/URLs.dart';
import 'package:chaos_chat_ui/chaos_chat_ui.dart';
import 'package:chaos/Utilities/API/LocalStorage.dart';

import 'Events.dart';

class RoomV2 {
  final String room_id;

  RoomV2(this.room_id){
    _room_name = room_id;
    _is_direct = false;
  }

  String _room_name;
  String get room_name => _room_name ?? room_id;
  set room_name(String value) {
    if (value != null) {
      _room_name = value;
      saveRoomData(room_id, "room_name", value);
    }
  }

  String _room_avatar;
  String get room_avatar => _room_avatar;
  set room_avatar(String value) {
    if(value != null) {
      String url = mxcToHttpUrl(value);
      _room_avatar = url;
    }else{
      _room_avatar = null;
    }
    saveRoomData(room_id, "room_avatar", _room_avatar);
  }

  bool _is_direct;
  bool get is_direct => _is_direct ?? false;
  set is_direct(bool value) {
    if (value != null) {
      _is_direct = value;
      saveRoomData(room_id, "is_direct", value);
    }
  }

  String _lastEvent;
  String get lastEvent => _lastEvent ?? '';
  set lastEvent(String value) {
    if (value != null) {
      _lastEvent = value;
      saveRoomData(room_id, "lastEvent", value);
    }
  }

  String _draft;
  String get draft => _draft ?? '';
  set draft(String value) {
    if (value != null) {
      _draft = value;
      saveRoomData(room_id, "draft", value);
    }
  }

  bool _draftPresent;
  bool get draftPresent => _draftPresent ?? false;
  set draftPresent(bool value) {
    if (value != null) {
      _draftPresent = value;
      saveRoomData(room_id, "draftPresent", value);
    }
  }

  List<ChatMessage> events = List<ChatMessage>.empty(growable: true);

  List<ChatMessage> _unsentEvents = List<ChatMessage>.empty(growable: true);
  List<ChatMessage> get unsentEvents => _unsentEvents;

  Future<void> load() async {
    _room_name = await loadRoomData(room_id, "room_name");
    _room_avatar = await loadRoomData(room_id, "room_avatar");
    _is_direct = await loadRoomData(room_id, "is_direct");
    events = await getMessagesFromLocalStorage(room_id);
    _lastEvent = await loadRoomData(room_id, "lastEvent");
    _draft = await loadRoomData(room_id, "draft");
    _draftPresent = await loadRoomData(room_id, "draftPresent");
  }

  Future<void> savePrevBatch(String prev_batch) async {
    await savePrevBatchToLocalStorage(room_id, prev_batch);
  }

  Future<void> updateRoomEvents(Map<String, dynamic> roomUpdates) async {

    List<dynamic> timelineUpdates = roomUpdates['timeline']['events'];
    
    await addEvents(timelineUpdates);

    lastEvent =  await getEventSummary(timelineUpdates.last);

  }

  Future<void> loadOlderEvents() async {

    List<dynamic> oldEvents = await getOlderRoomEvents(room_id);

    await addEvents(oldEvents, position: 0);
  }

  Future<void> addEvents(List<dynamic> newEvents, {int position}) async {
    
    for(dynamic eventJson in newEvents){
      RoomEventModel event = getEvent(eventJson);

      if(!handledEvents.contains(event.event_id)) {

        handledEvents.add(event.event_id);

        ChatMessage message = await _toChatEvent(event, old: position != null);
        if(message != null) {
          position == null ? events.add(message) : events.insert(position, message);
          saveMessagesListToLocalStorage(room_id, events);
        }
      }
    }

  }

  Future<ChatMessage> _toChatEvent(RoomEventModel event, {bool old=true}) async {

    switch (event.type) {
      case mRoomMessage:
        if (event.content['msgtype'] == mText) {
          return await event.toTextMessage();
        }else if(event.content['msgtype'] == 'm.image'){
          return await event.toImageMessage();
        }else if(event.content['msgtype'] == 'm.video'){
          return await event.toVideoMessage();
        }
        break;
      case mRoomMember:
        return await event.toChatEvent();
        break;
      case mRoomName:
        if(!old) room_name = event.content['name'];
        return await event.toChatEvent();
        break;
      case mRoomAvatar:
        if(!old) room_avatar = event.content['url'];
        return await event.toChatEvent();
        break;
    }

    return null;
  }

}