class JoinedMembersModel {
  final Map<String, dynamic> joinedMembers;

  JoinedMembersModel({this.joinedMembers});

  factory JoinedMembersModel.fromJson(Map<String, dynamic> json) {
    return JoinedMembersModel(
      joinedMembers: json['joined'],
    );
  }
}
