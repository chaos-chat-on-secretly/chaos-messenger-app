class SendMessageModel {
  final String event_id;

  SendMessageModel({ this.event_id});

  factory SendMessageModel.fromJson(Map<String, dynamic> json) {
    return SendMessageModel(
      event_id: json['event_id'],
    );
  }
}