class DashChatModel {
  final String id;
  final String text;
  final String image;
  final String video;
  final DateTime createdAt;
  final Object user;
  DashChatModel({ this.id,this.text,this.image,this.video,this.createdAt,this.user});

  factory DashChatModel.fromJson(Map<String, dynamic> json) {
    return DashChatModel(
        id: json['id'],
        text: json['text'],
        image:json['image'],
        video:json['video'],
        user:json['user']
    );
  }
}