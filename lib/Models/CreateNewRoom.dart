class CreateRoomModel {
  final String room_id;
  final String room_alias;

  CreateRoomModel({this.room_id, this.room_alias});

  factory CreateRoomModel.fromJson(Map<String, dynamic> json) {
    return CreateRoomModel(
      room_id: json['room_id'],
      room_alias: json['room_alias'],

    );
  }
}

