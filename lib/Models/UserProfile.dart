import 'package:chaos/Utilities/URLs.dart';

class UserProfile{
  String user_id;
  String displayname;
  String avatar_url;

  UserProfile({this.user_id, this.displayname, this.avatar_url}){
    displayname ??= user_id;
    avatar_url = mxcToHttpUrl(avatar_url);
  }

  factory UserProfile.fromJson(dynamic json) {

    if(json == null){
      return null;
    }

    String avatar_http_url = mxcToHttpUrl(json['avatar_url']);

    return UserProfile(
        user_id: json['user_id'],
        displayname: json['displayname'],
        avatar_url: avatar_http_url
    );
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = Map<String, dynamic>();

    try {
      data['user_id'] = this.user_id;
      data['displayname'] = this.displayname;
      data['avatar_url'] = this.avatar_url;
    } catch (e, stack) {
      print('ERROR caught when trying to convert UserProfile to JSON:');
      print(e);
      print(stack);
    }
    return data;
  }
}