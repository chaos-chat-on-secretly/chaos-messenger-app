class ErrorHandlingModel {
  final String errcode;
  final String error;

  ErrorHandlingModel({this.errcode, this.error});

  factory ErrorHandlingModel.fromJson(Map<String, dynamic> json) {
    return ErrorHandlingModel(
      errcode: json['errcode'],
      error: json['error'],

    );
  }
}