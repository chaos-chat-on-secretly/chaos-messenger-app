class AuthTypeModel {
  final Object flow;

  AuthTypeModel({this.flow});

  factory AuthTypeModel.fromJson(Map<String, dynamic> json) {
    return AuthTypeModel(
      flow: json['flows'][0]['type'],
    );
  }
}
