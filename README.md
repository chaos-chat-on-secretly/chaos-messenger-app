# Chaos : Chat Messaging Platform[]

A matrix client developed using flutter.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Build Instructions

1) Set your flutter path and dart SDK path in your IDE.
2) Execute the command: **flutter pub get** to get the dependencies.
3) Attach your devices to the IDE
4) Execute command: **flutter run -d *devicename*** or use the run button of IDE to build the app.

### For web:

1) Ensure you are using flutter **beta** channel. Change your channel [here](https://flutter.dev/docs/development/tools/sdk/upgrading#switching-flutter-channels).
2) Execute command: **flutter config --enable-web** and restart any IDE if open.
3) Select your *web server* in devices using IDE.
4) Execute command: **flutter run -d *devicename*** or use the run button of IDE to build the app.

#### Possible Errors

- If you get errors saying invalid package name just rename your cloned repo folder to *chaos_messenger_app*.


